﻿using System;
using System.Drawing;
using System.Globalization;
using System.Windows.Forms;

namespace TrianglePuzzleGui
{

    /// <summary>
    /// User control that represents a position-jump puzzle position.
    /// </summary>
    public partial class PositionControl : UserControl
    {

        #region Fields

        /// <summary>
        /// Index of the position.
        /// </summary>
        int index;

        /// <summary>
        /// Font to use to display the index name.
        /// </summary>
        Font indexFont;

        /// <summary>
        /// Text to display.
        /// </summary>
        string indexText;

        /// <summary>
        /// Bounding rectangle for the inner circle that is drawn when occupied.
        /// </summary>
        Rectangle innerRectangle;

        /// <summary>
        /// True when the position should be drawn as occupied.
        /// </summary>
        bool occupied;

        /// <summary>
        /// Bounding rectangle for the outer circle that is always drawn.
        /// </summary>
        Rectangle outerRectangle;

        /// <summary>
        /// Bounding rectangle for the text to be displayed.
        /// </summary>
        Rectangle textRectangle;

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor that takes no arguments.
        /// </summary>
        public PositionControl()
        {
            InitializeComponent();
            outerRectangle = new Rectangle(1, 1, 29, 29);
            innerRectangle = new Rectangle(5, 5, 21, 21);
            textRectangle = new Rectangle(11, 9, 10, 12);
            indexFont = new Font("Microsoft Sans Serif", 8.25f);
        }

        /// <summary>
        /// Constructor where the position index is passed in.
        /// </summary>
        /// <param name="index">Index of the position.</param>
        public PositionControl(int index)
            : this()
        {
            Index = index;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Index of this position.
        /// </summary>
        public int Index 
        {
            get { return index; }
            set
            {
                index = value;
                indexText = index.ToString("X", CultureInfo.CurrentCulture);
                Invalidate();
            }
        }

        /// <summary>
        /// Get/Sets whether the position is occupied or not.
        /// </summary>
        public bool Occupied 
        {
            get { return occupied; }
            set
            {
                occupied = value;
                Invalidate();
            }
        }

        #endregion

        #region Control Events

        /// <summary>
        /// Event handler for the Load event, just invalidates the control so it gets repainted.
        /// </summary>
        /// <param name="sender">Sender object.</param>
        /// <param name="e">Event args.</param>
        private void PositionControl_Load(object sender, EventArgs e)
        {
            Invalidate();
        }

        /// <summary>
        /// Event handler for the click event, toggles the occupied state and invalidates the control.
        /// </summary>
        /// <param name="sender">Sender object.</param>
        /// <param name="e">Event args.</param>
        private void PositionControl_Click(object sender, EventArgs e)
        {
            occupied = !Occupied;
            Invalidate();
        }

        /// <summary>
        /// Event handler for the paint event, actually does the drawing of the parts of the control.
        /// </summary>
        /// <param name="sender">Sender object.</param>
        /// <param name="e">Event args.</param>
        private void PositionControl_Paint(object sender, PaintEventArgs e)
        {
            using (Pen pen = new Pen(Color.Black))
            {
                e.Graphics.DrawEllipse(pen, outerRectangle);
                if (Occupied)
                {
                    e.Graphics.DrawEllipse(pen, innerRectangle);
                    e.Graphics.FillEllipse(pen.Brush, innerRectangle);
                    using (Pen textPen = new Pen(Color.White))
                    {
                        e.Graphics.DrawString(indexText,
                                              indexFont,
                                              textPen.Brush,
                                              textRectangle);
                    }
                }
                else
                {
                    e.Graphics.DrawString(indexText,
                                          indexFont,
                                          pen.Brush,
                                          textRectangle);
                }
            }
        }

        #endregion

        #region Override Methods

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            if (disposing && (indexFont != null))
            {
                indexFont.Dispose();
                indexFont = null;
            }
            base.Dispose(disposing);
        }

        #endregion

    }

}
