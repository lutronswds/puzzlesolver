﻿namespace TrianglePuzzleGui
{
    partial class PositionControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // PositionControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "PositionControl";
            this.Size = new System.Drawing.Size(31, 31);
            this.Load += new System.EventHandler(this.PositionControl_Load);
            this.Click += new System.EventHandler(this.PositionControl_Click);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.PositionControl_Paint);
            this.ResumeLayout(false);

        }

        #endregion

    }
}
