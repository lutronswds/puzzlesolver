﻿namespace TrianglePuzzleGui
{
    partial class TrianglePuzzleForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.resultsListBox = new System.Windows.Forms.ListBox();
            this.solveButton = new System.Windows.Forms.Button();
            this.resultsLabel = new System.Windows.Forms.Label();
            this.endConditionGroupBox = new System.Windows.Forms.GroupBox();
            this.piecesLeftWithNoMovesRadioButton = new System.Windows.Forms.RadioButton();
            this.onePieceLeftInStartingPositionRadioButton = new System.Windows.Forms.RadioButton();
            this.onePieceLeftRadioButton = new System.Windows.Forms.RadioButton();
            this.resetButton = new System.Windows.Forms.Button();
            this.piecesLeftComboBox = new System.Windows.Forms.ComboBox();
            this.endConditionGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // resultsListBox
            // 
            this.resultsListBox.FormattingEnabled = true;
            this.resultsListBox.Location = new System.Drawing.Point(348, 25);
            this.resultsListBox.Name = "resultsListBox";
            this.resultsListBox.Size = new System.Drawing.Size(127, 342);
            this.resultsListBox.TabIndex = 0;
            this.resultsListBox.SelectedIndexChanged += new System.EventHandler(this.resultsListBox_SelectedIndexChanged);
            // 
            // solveButton
            // 
            this.solveButton.Location = new System.Drawing.Point(233, 347);
            this.solveButton.Name = "solveButton";
            this.solveButton.Size = new System.Drawing.Size(75, 23);
            this.solveButton.TabIndex = 1;
            this.solveButton.Text = "Solve";
            this.solveButton.UseVisualStyleBackColor = true;
            this.solveButton.Click += new System.EventHandler(this.solveButton_Click);
            // 
            // resultsLabel
            // 
            this.resultsLabel.AutoSize = true;
            this.resultsLabel.Location = new System.Drawing.Point(345, 9);
            this.resultsLabel.Name = "resultsLabel";
            this.resultsLabel.Size = new System.Drawing.Size(42, 13);
            this.resultsLabel.TabIndex = 2;
            this.resultsLabel.Text = "Results";
            // 
            // endConditionGroupBox
            // 
            this.endConditionGroupBox.Controls.Add(this.piecesLeftComboBox);
            this.endConditionGroupBox.Controls.Add(this.piecesLeftWithNoMovesRadioButton);
            this.endConditionGroupBox.Controls.Add(this.onePieceLeftInStartingPositionRadioButton);
            this.endConditionGroupBox.Controls.Add(this.onePieceLeftRadioButton);
            this.endConditionGroupBox.Location = new System.Drawing.Point(12, 205);
            this.endConditionGroupBox.Name = "endConditionGroupBox";
            this.endConditionGroupBox.Size = new System.Drawing.Size(205, 94);
            this.endConditionGroupBox.TabIndex = 3;
            this.endConditionGroupBox.TabStop = false;
            this.endConditionGroupBox.Text = "End Condition";
            // 
            // piecesLeftWithNoMovesRadioButton
            // 
            this.piecesLeftWithNoMovesRadioButton.AutoSize = true;
            this.piecesLeftWithNoMovesRadioButton.Location = new System.Drawing.Point(7, 67);
            this.piecesLeftWithNoMovesRadioButton.Name = "piecesLeftWithNoMovesRadioButton";
            this.piecesLeftWithNoMovesRadioButton.Size = new System.Drawing.Size(145, 17);
            this.piecesLeftWithNoMovesRadioButton.TabIndex = 2;
            this.piecesLeftWithNoMovesRadioButton.TabStop = true;
            this.piecesLeftWithNoMovesRadioButton.Text = "Pieces left with no moves";
            this.piecesLeftWithNoMovesRadioButton.UseVisualStyleBackColor = true;
            // 
            // onePieceLeftInStartingPositionRadioButton
            // 
            this.onePieceLeftInStartingPositionRadioButton.AutoSize = true;
            this.onePieceLeftInStartingPositionRadioButton.Location = new System.Drawing.Point(7, 43);
            this.onePieceLeftInStartingPositionRadioButton.Name = "onePieceLeftInStartingPositionRadioButton";
            this.onePieceLeftInStartingPositionRadioButton.Size = new System.Drawing.Size(178, 17);
            this.onePieceLeftInStartingPositionRadioButton.TabIndex = 1;
            this.onePieceLeftInStartingPositionRadioButton.TabStop = true;
            this.onePieceLeftInStartingPositionRadioButton.Text = "One piece left in starting position";
            this.onePieceLeftInStartingPositionRadioButton.UseVisualStyleBackColor = true;
            // 
            // onePieceLeftRadioButton
            // 
            this.onePieceLeftRadioButton.AutoSize = true;
            this.onePieceLeftRadioButton.Location = new System.Drawing.Point(6, 19);
            this.onePieceLeftRadioButton.Name = "onePieceLeftRadioButton";
            this.onePieceLeftRadioButton.Size = new System.Drawing.Size(91, 17);
            this.onePieceLeftRadioButton.TabIndex = 0;
            this.onePieceLeftRadioButton.TabStop = true;
            this.onePieceLeftRadioButton.Text = "One piece left";
            this.onePieceLeftRadioButton.UseVisualStyleBackColor = true;
            // 
            // resetButton
            // 
            this.resetButton.Location = new System.Drawing.Point(0, 0);
            this.resetButton.Name = "resetButton";
            this.resetButton.Size = new System.Drawing.Size(75, 23);
            this.resetButton.TabIndex = 4;
            this.resetButton.Text = "Reset";
            this.resetButton.UseVisualStyleBackColor = true;
            this.resetButton.Click += new System.EventHandler(this.resetButton_Click);
            // 
            // piecesLeftComboBox
            // 
            this.piecesLeftComboBox.FormattingEnabled = true;
            this.piecesLeftComboBox.Location = new System.Drawing.Point(158, 66);
            this.piecesLeftComboBox.Name = "piecesLeftComboBox";
            this.piecesLeftComboBox.Size = new System.Drawing.Size(40, 21);
            this.piecesLeftComboBox.TabIndex = 3;
            this.piecesLeftComboBox.SelectedIndexChanged += new System.EventHandler(this.piecesLeftComboBox_SelectedIndexChanged);
            // 
            // TrianglePuzzleForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(487, 382);
            this.Controls.Add(this.resetButton);
            this.Controls.Add(this.endConditionGroupBox);
            this.Controls.Add(this.resultsLabel);
            this.Controls.Add(this.solveButton);
            this.Controls.Add(this.resultsListBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "TrianglePuzzleForm";
            this.Text = "Triangle Puzzle";
            this.Load += new System.EventHandler(this.TrianglePuzzleForm_Load);
            this.endConditionGroupBox.ResumeLayout(false);
            this.endConditionGroupBox.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox resultsListBox;
        private System.Windows.Forms.Button solveButton;
        private System.Windows.Forms.Label resultsLabel;
        private System.Windows.Forms.GroupBox endConditionGroupBox;
        private System.Windows.Forms.RadioButton piecesLeftWithNoMovesRadioButton;
        private System.Windows.Forms.RadioButton onePieceLeftInStartingPositionRadioButton;
        private System.Windows.Forms.RadioButton onePieceLeftRadioButton;
        private System.Windows.Forms.Button resetButton;
        private System.Windows.Forms.ComboBox piecesLeftComboBox;
    }
}

