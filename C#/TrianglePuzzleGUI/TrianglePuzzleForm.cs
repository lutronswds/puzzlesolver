﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;

using PuzzleSolver.Engines;
using PuzzleSolver.PositionJumpPuzzleLibrary;

namespace TrianglePuzzleGui
{

    /// <summary>
    /// Class for the triangle puzzle form
    /// </summary>
    public partial class TrianglePuzzleForm : Form
    {

        #region Constants

        /// <summary>
        /// Center point for arranging the position controls.
        /// </summary>
        const int drawingCenterPoint = 100;

        /// <summary>
        /// Horizontal gap used when arranging the controls.
        /// </summary>
        const int horizontalGap = 2;
        
        /// <summary>
        /// Number of positions used for looping and bounds checking.
        /// </summary>
        const int numberOfPositions = 0xF;
        
        /// <summary>
        /// Vertical gap used when arranging the controls.
        /// </summary>        
        const int verticallGap = 5;

        #endregion

        #region Fields

        /// <summary>
        /// Lookup table of position description to position control.
        /// </summary>
        Dictionary<string, PositionControl> positionControlsLookup;

        /// <summary>
        /// Array of the created position controls.
        /// </summary>
        PositionControl[] positionControls;

        /// <summary>
        /// Array of bools indicating which positions are occupied.
        /// </summary>
        bool[] startingPositions;

        #endregion

        #region Constructor

        /// <summary>
        /// Default form constructor
        /// </summary>
        public TrianglePuzzleForm()
        {
            InitializeComponent();
        }

        #endregion

        #region Form Events

        /// <summary>
        /// Event handler for the load event, initializes and positions all of the controls on the form.
        /// </summary>
        /// <param name="sender">Sender object.</param>
        /// <param name="e">Event args.</param>
        private void TrianglePuzzleForm_Load(object sender, EventArgs e)
        {
            CreateControls();
        }

        #endregion

        #region Control Events

        /// <summary>
        /// Event handler for when the pieces left combo box changes, just runs the position click method.
        /// </summary>
        /// <param name="sender">Sender object.</param>
        /// <param name="e">Event args.</param>
        private void piecesLeftComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            positionControl_Click(sender, e);
        }

        /// <summary>
        /// Event handler for when a position control is clicked, disables the possible end of puzzle
        /// options based on the state of the puzzle board.
        /// </summary>
        /// <param name="sender">Sender object.</param>
        /// <param name="e">Event args.</param>
        void positionControl_Click(object sender, EventArgs e)
        {
            int occupiedControls = positionControls.Count(positionControl => positionControl.Occupied);
            
            if (occupiedControls == 0 || occupiedControls == numberOfPositions)
            {
                onePieceLeftRadioButton.Enabled = false;
                onePieceLeftInStartingPositionRadioButton.Enabled = false;
                piecesLeftWithNoMovesRadioButton.Enabled = false;
                solveButton.Enabled = false;
                return;
            }

            onePieceLeftRadioButton.Enabled = true;
            solveButton.Enabled = true;

            if (occupiedControls == (numberOfPositions - 1))
            {
                onePieceLeftInStartingPositionRadioButton.Enabled = true;
            }
            else
            {
                if (onePieceLeftInStartingPositionRadioButton.Checked)
                {
                    onePieceLeftRadioButton.Checked = true;
                }
                onePieceLeftInStartingPositionRadioButton.Enabled = false;
            }

            if (occupiedControls > (int)piecesLeftComboBox.SelectedItem)
            {
                piecesLeftWithNoMovesRadioButton.Enabled = true;
            }
            else
            {
                if (piecesLeftWithNoMovesRadioButton.Checked)
                {
                    onePieceLeftRadioButton.Checked = true;
                }
                piecesLeftWithNoMovesRadioButton.Enabled = false;
            }

        }

        /// <summary>
        /// Resets the positions to their default state (the first one unoccupied, the rest occupied).
        /// </summary>
        /// <param name="sender">Sender object.</param>
        /// <param name="e">Event args.</param>
        private void resetButton_Click(object sender, EventArgs e)
        {
            positionControls[0].Occupied = false;
            for (int index = 1; index < positionControls.Length; index++)
            {
                positionControls[index].Occupied = true;
            }
        }

        /// <summary>
        /// When a result move is clicked on, update the puzzle positions by showing all of
        /// the previous moves in the list.
        /// </summary>
        /// <param name="sender">Sender object.</param>
        /// <param name="e">Event args.</param>
        private void resultsListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            for (int index = 0; index <= resultsListBox.SelectedIndex; index++)
            {
                if (index == 0)
                {
                    InitializeDisplay();
                }
                else
                {
                    ShowMove((IPositionJumpPuzzleMove)resultsListBox.Items[index]);
                }
            }
        }

        /// <summary>
        /// Actually runs the puzzle engine to try to get a solution. If there is no solution a message box
        /// is displayed.
        /// </summary>
        /// <param name="sender">Sender object.</param>
        /// <param name="e">Event args.</param>
        private void solveButton_Click(object sender, EventArgs e)
        {
            resultsListBox.Items.Clear();

            startingPositions = positionControls.Select(positionControl => positionControl.Occupied).ToArray();

            IPositionJumpPuzzleBoard puzzleBoard = PositionJumpPuzzleFactory.CreateTrianglePuzzleBoard(startingPositions);

            IFirstSolutionPuzzleSolverEngine engine = PuzzleSolverEngineFactory.CreateFirstSolutionPuzzleSolverEngine();

            IEndOfPuzzleEvaluator endOfPuzzleEvaluator = null;
            if (onePieceLeftInStartingPositionRadioButton.Checked)
            {
                endOfPuzzleEvaluator = PositionJumpPuzzleFactory.CreateOnePieceInStartingPositionEndOfPuzzleEvaluator(puzzleBoard.PuzzleBoardPositions[positionControls.Where(positionControl => !positionControl.Occupied).FirstOrDefault().Index]);
            }
            else if (piecesLeftWithNoMovesRadioButton.Checked)
            {
                endOfPuzzleEvaluator = PositionJumpPuzzleFactory.CreatePiecesWithNoPossibleMovesEndOfPuzzleEvaluator((int)piecesLeftComboBox.SelectedItem);
            }
            else
            {
                endOfPuzzleEvaluator = PositionJumpPuzzleFactory.CreateOnePieceLeftEndOfPuzzleEvaluator();
            }

            solveButton.Enabled = false;
            Application.DoEvents();

            IEnumerable<IPuzzleMove> puzzleMoves = engine.Execute(puzzleBoard,
                                                                  endOfPuzzleEvaluator);

            solveButton.Enabled = true;
            Application.DoEvents();

            int numberOfMoves = 0;
            foreach (IPuzzleMove puzzleMove in puzzleMoves)
            {
                resultsListBox.Items.Add(puzzleMove);
                numberOfMoves++;
            }

            if (numberOfMoves == 0)
            {
                MessageBox.Show("No solution found.");
            }
            else
            {
                resultsListBox.Items.Insert(0, "<Start>");
            }
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Creates the controls and positions then correctly.
        /// </summary>
        void CreateControls()
        {
            positionControls = new PositionControl[numberOfPositions];
            positionControlsLookup = new Dictionary<string, PositionControl>();
            for (int index = 0; index < numberOfPositions; index++)
            {
                PositionControl positionControl = new PositionControl(index);
                positionControl.Click += new EventHandler(positionControl_Click);
                positionControls[index] = positionControl;
                positionControlsLookup.Add(index.ToString("X", CultureInfo.CurrentCulture), positionControl);
                positionControl.Parent = this;
                positionControl.Occupied = true;
                switch (index)
                {
                    case 0x0:
                        positionControl.Left = drawingCenterPoint;
                        positionControl.Top = 0;
                        positionControl.Occupied = false;
                        break;
                    case 0x1:
                        positionControl.Left = drawingCenterPoint - (positionControl.Width / 2) - horizontalGap;
                        positionControl.Top = positionControl.Height + verticallGap;
                        break;
                    case 0x2:
                        positionControl.Left = drawingCenterPoint + (positionControl.Width / 2) + horizontalGap;
                        positionControl.Top = positionControl.Height + verticallGap;
                        break;
                    case 0x3:
                        positionControl.Left = drawingCenterPoint - positionControl.Width - horizontalGap;
                        positionControl.Top = 2 * (positionControl.Height + verticallGap);
                        break;
                    case 0x4:
                        positionControl.Left = drawingCenterPoint;
                        positionControl.Top = 2 * (positionControl.Height + verticallGap);
                        break;
                    case 0x5:
                        positionControl.Left = drawingCenterPoint + positionControl.Width + horizontalGap;
                        positionControl.Top = 2 * (positionControl.Height + verticallGap);
                        break;
                    case 0x6:
                        positionControl.Left = drawingCenterPoint - (positionControl.Width / 2) - positionControl.Width - (2 * horizontalGap);
                        positionControl.Top = 3 * (positionControl.Height + verticallGap);
                        break;
                    case 0x7:
                        positionControl.Left = drawingCenterPoint - (positionControl.Width / 2) - horizontalGap;
                        positionControl.Top = 3 * (positionControl.Height + verticallGap);
                        break;
                    case 0x8:
                        positionControl.Left = drawingCenterPoint + (positionControl.Width / 2) + horizontalGap;
                        positionControl.Top = 3 * (positionControl.Height + verticallGap);
                        break;
                    case 0x9:
                        positionControl.Left = drawingCenterPoint + (positionControl.Width / 2) + positionControl.Width + (2 * horizontalGap);
                        positionControl.Top = 3 * (positionControl.Height + verticallGap);
                        break;
                    case 0xA:
                        positionControl.Left = drawingCenterPoint - (2 * positionControl.Width) - (2 * horizontalGap);
                        positionControl.Top = 4 * (positionControl.Height + verticallGap);
                        break;
                    case 0xB:
                        positionControl.Left = drawingCenterPoint - positionControl.Width - horizontalGap;
                        positionControl.Top = 4 * (positionControl.Height + verticallGap);
                        break;
                    case 0xC:
                        positionControl.Left = drawingCenterPoint;
                        positionControl.Top = 4 * (positionControl.Height + verticallGap);
                        break;
                    case 0xD:
                        positionControl.Left = drawingCenterPoint + positionControl.Width + horizontalGap;
                        positionControl.Top = 4 * (positionControl.Height + verticallGap);
                        break;
                    case 0xE:
                        positionControl.Left = drawingCenterPoint + (2 * positionControl.Width) + (2 * horizontalGap);
                        positionControl.Top = 4 * (positionControl.Height + verticallGap);
                        break;
                }
            }
            onePieceLeftRadioButton.Checked = true;
            endConditionGroupBox.Width = 205;
            endConditionGroupBox.Left = (drawingCenterPoint + (positionControls[0xA].Width / 2)) - (endConditionGroupBox.Width / 2);
            endConditionGroupBox.Top = positionControls[0xA].Bottom + 10;
            for (int index = 2; index < numberOfPositions; index++)
            {
                piecesLeftComboBox.Items.Add(index);
            }
            piecesLeftComboBox.SelectedIndex = 0;
            solveButton.Left = endConditionGroupBox.Left;
            solveButton.Top = endConditionGroupBox.Bottom + 10;
            solveButton.Width = endConditionGroupBox.Width;
            resultsLabel.Left = endConditionGroupBox.Right + positionControls[0xA].Left;
            resultsListBox.Left = resultsLabel.Left;
            resultsListBox.Height = solveButton.Bottom - resultsListBox.Top;
            Width = resultsListBox.Right + positionControls[0xA].Left;
            Height = solveButton.Bottom + 50;
        }

        /// <summary>
        /// Sets the position controls to the last starting position used.
        /// </summary>
        void InitializeDisplay()
        {
            for (int index = 0; index < numberOfPositions; index++)
            {
                positionControls[index].Occupied = startingPositions[index];
            }
        }

        /// <summary>
        /// Updates the appropriate puzzle position with the values in the move.
        /// </summary>
        /// <param name="puzzleMove">Move to update the positions with.</param>
        void ShowMove(IPositionJumpPuzzleMove puzzleMove)
        {
            positionControlsLookup[puzzleMove.StartPosition.Description].Occupied = false;
            positionControlsLookup[puzzleMove.MiddlePosition.Description].Occupied = false;
            positionControlsLookup[puzzleMove.EndPosition.Description].Occupied = true;
        }

        #endregion

        #region Override Methods

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            if (disposing && (positionControls != null))
            {
                foreach (PositionControl positionControl in positionControls)
                {
                    positionControl.Dispose();
                }
                positionControls = null;
            }
            base.Dispose(disposing);
        }

        #endregion

    }

}
