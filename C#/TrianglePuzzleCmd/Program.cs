﻿using System;

using PuzzleSolver.Engines;
using PuzzleSolver.PositionJumpPuzzleLibrary;
    
namespace TrianglePuzzleCmd
{

    /// <summary>
    /// Command line front-end for the triangle puzzle.
    /// </summary>
    class Program
    {

        /// <summary>
        /// Value to return when there is a problem parsing the command line arguments.
        /// </summary>
        const int invalidArgumentsReturnCode = -1;
        
        /// <summary>
        /// Value to return when no solution could be found to the puzzle.
        /// </summary>
        const int noSolutionReturnCode = -2;
        
        /// <summary>
        /// Value to return when a valid solution was found to the puzzle.
        /// </summary>
        const int validSolutionReturnCode = 0;

        /// <summary>
        /// Number of positions in the puzzle, used for validating the command-line arguments.
        /// </summary>
        const int numberOfPositions = 15;
        
        /// <summary>
        /// Main Program. This method parses the arguments, creates the needed objects, and then displays the solution.
        /// </summary>
        /// <param name="args">Command-line arguments to configure the execution.</param>
        /// <returns>One of the return code constants.</returns>
        static int Main(string[] args)
        {
            
            // Parse the command-line arguments.
            Configuration configuration = ParseArguments(args);
            if (configuration.EndOfPuzzleEvaluator == null)
            {
                Console.WriteLine("Press <Enter> to end");
                Console.ReadLine(); 
                return invalidArgumentsReturnCode;
            }

            // Create the puzzle board.
            IPositionJumpPuzzleBoard puzzleBoard = PositionJumpPuzzleFactory.CreateTrianglePuzzleBoard(configuration.StartingConfiguration);
            if (configuration.SetStartingPosition)
            {
                ((IStartingPositionHolder)configuration.EndOfPuzzleEvaluator).StartingPosition = puzzleBoard.PuzzleBoardPositions[configuration.StartingPositionIndex];
            }

            // Create the puzzle solver engine.
            IFirstSolutionPuzzleSolverEngine engine = PuzzleSolverEngineFactory.CreateFirstSolutionPuzzleSolverEngine();

            // Display the starting configuration of the board.
            Console.WriteLine("Starting Configuration:");
            Console.WriteLine(puzzleBoard.ToString());

            // Display the moves to get to the puzzle solution.
            int numberOfMoves = 0;
            foreach (IPuzzleMove puzzleMove in engine.Execute(puzzleBoard, configuration.EndOfPuzzleEvaluator))
            {
                Console.WriteLine(puzzleMove.ToString());
                numberOfMoves++;
            }
            
            Console.WriteLine();

            if (numberOfMoves == 0)
            {
                Console.WriteLine("No solution found");
                Console.WriteLine();
            }

            // Pause if configured.
            if (configuration.Pause)
            {
                Console.WriteLine("Press <Enter> to end");
                Console.ReadLine();
            }

            // Return the proper return code.
            if (numberOfMoves == 0)
            {
                return noSolutionReturnCode;
            }
            else
            {
                return validSolutionReturnCode;
            }
        }

        /// <summary>
        /// Parses the command-line arguments and populates a Configuration object with the values.
        /// </summary>
        /// <param name="args">Command-line arguments to parse.</param>
        /// <returns>A Configuration object with the parsed values. If a value is not specified the
        /// default value is set by the Configuration object.</returns>
        static Configuration ParseArguments(string[] args)
        {
            Configuration configuration = new Configuration();

            for (int argumentsIndex = 0; argumentsIndex < args.Length; argumentsIndex++)
            { 
                string arg = args[argumentsIndex].ToUpperInvariant();
                if (arg.StartsWith("-E", StringComparison.OrdinalIgnoreCase))
                {
                    // End goal
                    if (arg[2] == '1')
                    {
                        configuration.EndOfPuzzleEvaluator = PositionJumpPuzzleFactory.CreateOnePieceLeftEndOfPuzzleEvaluator();
                        configuration.SetStartingPosition = false;
                    }
                    else if (arg[2] == 'S')
                    {
                        configuration.EndOfPuzzleEvaluator = PositionJumpPuzzleFactory.CreateOnePieceInStartingPositionEndOfPuzzleEvaluator();
                        configuration.SetStartingPosition = true;
                    }
                    else
                    {
                        string numberText = arg.Substring(2);
                        int number;
                        if (string.IsNullOrWhiteSpace(numberText) ||
                            !int.TryParse(numberText, out number) ||
                            number <= 1 ||
                            number > numberOfPositions - 1)
                        {
                            DisplayHelpMessage();
                            return configuration;
                        }
                        else
                        {
                            configuration.EndOfPuzzleEvaluator = PositionJumpPuzzleFactory.CreatePiecesWithNoPossibleMovesEndOfPuzzleEvaluator(number);
                            configuration.SetStartingPosition = false;
                        }
                    }
                }
                else if (arg.StartsWith("-P", StringComparison.OrdinalIgnoreCase))
                {
                    // Pause after running
                    configuration.Pause = true;
                }
                else if (arg.StartsWith("-NP", StringComparison.OrdinalIgnoreCase))
                {
                    // Don't pause after running
                    configuration.Pause = false;
                }
                else if (arg.StartsWith("-S", StringComparison.OrdinalIgnoreCase))
                {
                    // Starting configuration
                    string startPositionText = arg.Substring(2);
                    if (startPositionText.Length != numberOfPositions)
                    {
                        DisplayHelpMessage();
                        return configuration;
                    }
                    else
                    {
                        configuration.StartingConfiguration = new bool[numberOfPositions];
                        for (int index = 0; index < numberOfPositions; index++)
                        {
                            if (startPositionText[index] == '0')
                            {
                                configuration.StartingConfiguration[index] = false;
                            }
                            else
                            {
                                configuration.StartingConfiguration[index] = true;
                            }
                        }
                    }
                }
                else
                {
                    DisplayHelpMessage();
                    return configuration;
                }
            }

            // Calculate the starting position index if needed
            if (configuration.SetStartingPosition && configuration.StartingConfiguration != null)
            {
                int emptyPositionCount = 0;
                int emptyPositionIndex = 0;
                for (int index = 0; index < configuration.StartingConfiguration.Length; index++)
                {
                    if (!configuration.StartingConfiguration[index])
                    {
                        emptyPositionCount++;
                        emptyPositionIndex = index;
                    }
                }
                if (emptyPositionCount != 1)
                {
                    DisplayHelpMessage();
                    return configuration;
                }
                else
                {
                    configuration.StartingPositionIndex = emptyPositionIndex;
                }
            }

            // Default to the one-piece-left evaluator
            if (configuration.EndOfPuzzleEvaluator == null)
            {
                configuration.EndOfPuzzleEvaluator = PositionJumpPuzzleFactory.CreateOnePieceLeftEndOfPuzzleEvaluator();
            }

            return configuration;

        }

        /// <summary>
        /// Displays a message to the user listing command-line arguments.
        /// </summary>
        static void DisplayHelpMessage()
        {
            Console.WriteLine("Usage: TrianglePuzzleCmd [-e[1|S|<n>] [-p|-np] [-s<x>]");
            Console.WriteLine(" -e1 = Solve for one piece left");
            Console.WriteLine(" -eS = Solve for one piece left in the starting position");
            Console.WriteLine("       (must have one and only one empty position at start)");
            Console.WriteLine(" -e<n> = Solve for <n> number of pieces left with no possible moves");
            Console.WriteLine("         where <n> is a number between 2 and 14");
            Console.WriteLine(" -p = Pause after displaying results");
            Console.WriteLine(" -np = Do not pause after displaying results");
            Console.WriteLine(" -s<x> = Starting configuration, <x> is {0} 0s and 1s", numberOfPositions);
            Console.WriteLine("         0 means the position is empty, 1 means the position is occupied");
            Console.WriteLine("         the default starting configuration is 0{0}", new string('1', numberOfPositions - 1));
        }

        /// <summary>
        /// All of the configuration options possible. Default values are set in the constructor.
        /// </summary>
        class Configuration
        {

            /// <summary>
            /// Constructor that sets all of the default values.
            /// </summary>
            public Configuration()
            {
                // Default configuration values
                EndOfPuzzleEvaluator = null;
                // We want to pause be default if running in the IDE
                Pause = System.Diagnostics.Debugger.IsAttached;
                SetStartingPosition = false;
                StartingConfiguration = null;
                StartingPositionIndex = 0;
            }

            /// <summary>
            /// What end of end of puzzle evaluator to use.
            /// </summary>
            public IEndOfPuzzleEvaluator EndOfPuzzleEvaluator { get; set; }

            /// <summary>
            /// True to pause after displaying the solution, false to not pause.
            /// </summary>
            public bool Pause { get; set; }

            /// <summary>
            /// True if the starting position needs to be set, false if not.
            /// </summary>
            public bool SetStartingPosition { get; set; }

            /// <summary>
            /// Starting configuration. A true value means the position is occupied, 
            /// false means a position is unoccupied.
            /// </summary>
            public bool[] StartingConfiguration { get; set; }

            /// <summary>
            /// Index of the starting position.
            /// </summary>
            public int StartingPositionIndex { get; set; }

        }

    }

}
