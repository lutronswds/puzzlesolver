﻿using System.Collections.Generic;

namespace PuzzleSolver.Engines.Implementation
{

    /// <summary>
    /// Puzzle solver that uses a depth first search algorithm and stops when the first solution is found.
    /// </summary>
    class DfsFirstSolutionEngine : IFirstSolutionPuzzleSolverEngine
    {

        /// <summary>
        /// Solves a puzzle using a depth-first-search and returns the first solution found.
        /// </summary>
        /// <param name="puzzleBoard">Puzzle board to solve.</param>
        /// <param name="endOfPuzzleEvaluator">Object that can evaluate the puzzle board and determine if the goal has been reached or not.</param>
        /// <returns>A list of moves to acheive the goal. If there is no solution then an empty list is returned.</returns>
        public IList<IPuzzleMove> Execute(IPuzzleBoard puzzleBoard,
                                          IEndOfPuzzleEvaluator endOfPuzzleEvaluator)
        {
            List<IPuzzleMove> successfulMoves = new List<IPuzzleMove>();

            ExecuteRecursively(puzzleBoard, endOfPuzzleEvaluator, successfulMoves);

            return successfulMoves;
        }

        /// <summary>
        /// Method that solves the puzzle recursively by evaluating the puzzle board to see if the goal has been met or not.
        /// If the goal has not been met then all possible moves are executed recursively to see they can get to the goal. Once
        /// the goal has been acheived, moves that lead to the success are added the passed in list.
        /// </summary>
        /// <param name="puzzleBoard">Puzzle board to solve. Its state will be different on every call.</param>
        /// <param name="endOfPuzzleEvaluator">Object that can evaluate the puzzle board and determine if the goal has been reached or not.</param>
        /// <param name="successfulMoves">List to populate with the moves that led the acheivement of the goal.</param>
        /// <returns>True if the goal has been met, false otherwise.</returns>
        private bool ExecuteRecursively(IPuzzleBoard puzzleBoard,
                                        IEndOfPuzzleEvaluator endOfPuzzleEvaluator,
                                        List<IPuzzleMove> successfulMoves)
        {

            switch (endOfPuzzleEvaluator.Evaluate(puzzleBoard))
            {
                
                case EndOfPuzzlePossibility.Failure:
                    return false;
                
                case EndOfPuzzlePossibility.Success:
                    return true;
                
                default:
                    foreach (IPuzzleMove puzzleMove in puzzleBoard.GetPossibleMoves())
                    {
                        puzzleMove.Execute();
                        if (ExecuteRecursively(puzzleBoard, endOfPuzzleEvaluator, successfulMoves))
                        {
                            // Moves are always added to the front of the list because adding to the end
                            // would result in the list in the reverse order.
                            successfulMoves.Insert(0, puzzleMove);
                            return true;
                        }
                        else
                        {
                            // Undo the move so that the board is back in its initial state before
                            // trying the next move.
                            puzzleMove.Undo();
                        }
                    }
                    return false;

            }

        }

    }

}
