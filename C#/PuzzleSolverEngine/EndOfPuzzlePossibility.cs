﻿namespace PuzzleSolver.Engines
{

    /// <summary>
    /// Possible results when evaluating a puzzle to see if the goal has been reached.
    /// </summary>
    public enum EndOfPuzzlePossibility
    {

        /// <summary>
        /// The result is unknown, this should never happen
        /// </summary>
        Unknown = 0,

        /// <summary>
        /// The goal has not been reached and there are still moves that might acheive the goal.
        /// </summary>
        KeepPlaying,

        /// <summary>
        /// The goal has been reached.
        /// </summary>
        Success,

        /// <summary>
        /// The goal has not been reached and cannot be reached.
        /// </summary>
        Failure

    }

}
