﻿using PuzzleSolver.Engines.Implementation;

namespace PuzzleSolver.Engines
{

    /// <summary>
    /// Factory class to create objects in the Puzzle Solver Engine library on demand.
    /// </summary>
    public static class PuzzleSolverEngineFactory
    {

        /// <summary>
        /// Creates a puzzle solver engine that returns the first solution found.
        /// </summary>
        /// <returns>An object that implements the IFirstSolutionPuzzleSolverEngine interface.</returns>
        public static IFirstSolutionPuzzleSolverEngine CreateFirstSolutionPuzzleSolverEngine()
        {
            return new DfsFirstSolutionEngine();
        }

    }

}
