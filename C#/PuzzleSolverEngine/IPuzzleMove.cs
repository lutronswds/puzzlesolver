﻿namespace PuzzleSolver.Engines
{

    /// <summary>
    /// Interface for classes that represent one move on a puzzle board.
    /// </summary>
    public interface IPuzzleMove
    {

        /// <summary>
        /// Execute the move which will change the the state of the puzzle board.
        /// </summary>
        void Execute();

        /// <summary>
        /// Undo the execution of the move which will restore the state of the puzzle board.
        /// </summary>
        void Undo();

    }

}
