﻿using System.Collections.Generic;

namespace PuzzleSolver.Engines
{

    /// <summary>
    /// Interfaces for classes that can solve a puzzle and will return the first solution.
    /// </summary>
    public interface IFirstSolutionPuzzleSolverEngine
    {

        /// <summary>
        /// Solves the puzzle and returns the first valid solution found. If no
        /// solution is found an empty list is returned.
        /// </summary>
        /// <param name="puzzleBoard">Puzzle board to solve.</param>
        /// <param name="endOfPuzzleEvaluator">Object that can evaluate whether the goal has been acheived or not.</param>
        /// <returns>A list of moves to reach the goal. If the puzzle has no solution an empty list is returned.</returns>
        IList<IPuzzleMove> Execute(IPuzzleBoard puzzleBoard, 
                                   IEndOfPuzzleEvaluator endOfPuzzleEvaluator);

    }

}
