﻿using System.Collections.Generic;

namespace PuzzleSolver.Engines
{

    /// <summary>
    /// Interface for classes that hold puzzle state information for use by a puzzle solving engine.
    /// </summary>
    public interface IPuzzleBoard
    {

        /// <summary>
        /// Returns all possible moves based on the current state of the puzzle board.
        /// </summary>
        /// <returns>An enumerable set of moves. If there are no moves possible an empty set will be returned.</returns>
        IEnumerable<IPuzzleMove> GetPossibleMoves();

    }

}
