﻿namespace PuzzleSolver.Engines
{

    /// <summary>
    /// Interface for classes that can evaluate a puzzle board and determine if the goal has been met.
    /// </summary>
    public interface IEndOfPuzzleEvaluator
    {

        /// <summary>
        /// Evaluate function that will return if the goal has been met or not.
        /// </summary>
        /// <param name="puzzleBoard">Puzzle board to evalute.</param>
        /// <returns>A EndOfPuzzlePossiblity value that relates whether the goal has been met or not.</returns>
        EndOfPuzzlePossibility Evaluate(IPuzzleBoard puzzleBoard);

    }

}
