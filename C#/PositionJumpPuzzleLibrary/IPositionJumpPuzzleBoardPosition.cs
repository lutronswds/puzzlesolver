﻿namespace PuzzleSolver.PositionJumpPuzzleLibrary
{

    /// <summary>
    /// Interface for classes that represent a position on a position-jump puzzle board.
    /// </summary>
    public interface IPositionJumpPuzzleBoardPosition
    {

        /// <summary>
        /// Description of the board position to be used for display purposes.
        /// </summary>
        string Description { get; }

        /// <summary>
        /// Whether the position is currently occupied or not.
        /// </summary>
        bool Occupied { get; set; }

    }

}
