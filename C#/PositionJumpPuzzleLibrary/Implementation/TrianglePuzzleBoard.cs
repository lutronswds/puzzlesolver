﻿using System.Collections.Generic;
using System.Globalization;

namespace PuzzleSolver.PositionJumpPuzzleLibrary.Implementation
{

    //          0
    //        1   2
    //      3   4   5
    //    6   7   8   9
    //  A   B   C   D   E

    /// <summary>
    /// Puzzle board where the positions are arranged in a triangle.
    /// </summary>
    class TrianglePuzzleBoard : PositionJumpPuzzleBoardBase
    {

        #region Constructor

        /// <summary>
        /// Constructor using the default starting positions.
        /// </summary>
        public TrianglePuzzleBoard()
            : base()
        {
            // Intentionally empty
        }

        #endregion

        #region Override Properties

        /// <summary>
        /// Implementation of the number of puzzle board positions property.
        /// </summary>
        protected override int NumberOfPuzzleBoardPositions 
        {
            get { return 0xF; }
        }

        #endregion

        #region Override Methods

        /// <summary>
        /// Creates the puzzle board positions, the possible moves for each position, and sets up the initial state of the puzzle.
        /// </summary>
        /// <param name="occupiedPositions">Starting positions to use.</param>
        public override void Initialize(IList<bool> occupiedPositions)
        {
            PuzzleBoardPositions = new NumberPositionJumpPuzzleBoardPosition[NumberOfPuzzleBoardPositions];
            for (int index = 0; index < NumberOfPuzzleBoardPositions; index++)
            {
                PuzzleBoardPositions[index] = new NumberPositionJumpPuzzleBoardPosition(index);
            }

            AllPossibleMoves = new List<PositionJumpPuzzleMove>()
            {
                // starting from position 0
                new PositionJumpPuzzleMove(PuzzleBoardPositions[0x0], PuzzleBoardPositions[0x1], PuzzleBoardPositions[0x3]),
                new PositionJumpPuzzleMove(PuzzleBoardPositions[0x0], PuzzleBoardPositions[0x2], PuzzleBoardPositions[0x5]),
                // starting from position 1
                new PositionJumpPuzzleMove(PuzzleBoardPositions[0x1], PuzzleBoardPositions[0x3], PuzzleBoardPositions[0x6]),
                new PositionJumpPuzzleMove(PuzzleBoardPositions[0x1], PuzzleBoardPositions[0x4], PuzzleBoardPositions[0x8]),
                // starting from position 2
                new PositionJumpPuzzleMove(PuzzleBoardPositions[0x2], PuzzleBoardPositions[0x4], PuzzleBoardPositions[0x7]),
                new PositionJumpPuzzleMove(PuzzleBoardPositions[0x2], PuzzleBoardPositions[0x5], PuzzleBoardPositions[0x9]),
                // starting from position 3
                new PositionJumpPuzzleMove(PuzzleBoardPositions[0x3], PuzzleBoardPositions[0x1], PuzzleBoardPositions[0x0]),
                new PositionJumpPuzzleMove(PuzzleBoardPositions[0x3], PuzzleBoardPositions[0x4], PuzzleBoardPositions[0x5]),
                new PositionJumpPuzzleMove(PuzzleBoardPositions[0x3], PuzzleBoardPositions[0x6], PuzzleBoardPositions[0xA]),
                new PositionJumpPuzzleMove(PuzzleBoardPositions[0x3], PuzzleBoardPositions[0x7], PuzzleBoardPositions[0xC]),
                // starting from position 4
                new PositionJumpPuzzleMove(PuzzleBoardPositions[0x4], PuzzleBoardPositions[0x7], PuzzleBoardPositions[0xB]),
                new PositionJumpPuzzleMove(PuzzleBoardPositions[0x4], PuzzleBoardPositions[0x8], PuzzleBoardPositions[0xD]),
                // starting from position 5
                new PositionJumpPuzzleMove(PuzzleBoardPositions[0x5], PuzzleBoardPositions[0x2], PuzzleBoardPositions[0x0]),
                new PositionJumpPuzzleMove(PuzzleBoardPositions[0x5], PuzzleBoardPositions[0x4], PuzzleBoardPositions[0x3]),
                new PositionJumpPuzzleMove(PuzzleBoardPositions[0x5], PuzzleBoardPositions[0x8], PuzzleBoardPositions[0xC]),
                new PositionJumpPuzzleMove(PuzzleBoardPositions[0x5], PuzzleBoardPositions[0x9], PuzzleBoardPositions[0xE]),
                // starting from position 6
                new PositionJumpPuzzleMove(PuzzleBoardPositions[0x6], PuzzleBoardPositions[0x3], PuzzleBoardPositions[0x1]),
                new PositionJumpPuzzleMove(PuzzleBoardPositions[0x6], PuzzleBoardPositions[0x7], PuzzleBoardPositions[0x8]),
                // starting from position 7
                new PositionJumpPuzzleMove(PuzzleBoardPositions[0x7], PuzzleBoardPositions[0x4], PuzzleBoardPositions[0x2]),
                new PositionJumpPuzzleMove(PuzzleBoardPositions[0x7], PuzzleBoardPositions[0x8], PuzzleBoardPositions[0x9]),
                // starting from position 8
                new PositionJumpPuzzleMove(PuzzleBoardPositions[0x8], PuzzleBoardPositions[0x4], PuzzleBoardPositions[0x1]),
                new PositionJumpPuzzleMove(PuzzleBoardPositions[0x8], PuzzleBoardPositions[0x7], PuzzleBoardPositions[0x6]),
                // starting from position 9
                new PositionJumpPuzzleMove(PuzzleBoardPositions[0x9], PuzzleBoardPositions[0x5], PuzzleBoardPositions[0x2]),
                new PositionJumpPuzzleMove(PuzzleBoardPositions[0x9], PuzzleBoardPositions[0x8], PuzzleBoardPositions[0x7]),
                // starting from position A
                new PositionJumpPuzzleMove(PuzzleBoardPositions[0xA], PuzzleBoardPositions[0x6], PuzzleBoardPositions[0x3]),
                new PositionJumpPuzzleMove(PuzzleBoardPositions[0xA], PuzzleBoardPositions[0xB], PuzzleBoardPositions[0xC]),
                // starting from position B
                new PositionJumpPuzzleMove(PuzzleBoardPositions[0xB], PuzzleBoardPositions[0x7], PuzzleBoardPositions[0x4]),
                new PositionJumpPuzzleMove(PuzzleBoardPositions[0xB], PuzzleBoardPositions[0xC], PuzzleBoardPositions[0xD]),
                // starting from position C
                new PositionJumpPuzzleMove(PuzzleBoardPositions[0xC], PuzzleBoardPositions[0x7], PuzzleBoardPositions[0x3]),
                new PositionJumpPuzzleMove(PuzzleBoardPositions[0xC], PuzzleBoardPositions[0x8], PuzzleBoardPositions[0x5]),
                new PositionJumpPuzzleMove(PuzzleBoardPositions[0xC], PuzzleBoardPositions[0xB], PuzzleBoardPositions[0xA]),
                new PositionJumpPuzzleMove(PuzzleBoardPositions[0xC], PuzzleBoardPositions[0xD], PuzzleBoardPositions[0xE]),
                // starting from position D
                new PositionJumpPuzzleMove(PuzzleBoardPositions[0xD], PuzzleBoardPositions[0x8], PuzzleBoardPositions[0x4]),
                new PositionJumpPuzzleMove(PuzzleBoardPositions[0xD], PuzzleBoardPositions[0xC], PuzzleBoardPositions[0xB]),
                // starting from position E
                new PositionJumpPuzzleMove(PuzzleBoardPositions[0xE], PuzzleBoardPositions[0x9], PuzzleBoardPositions[0x5]),
                new PositionJumpPuzzleMove(PuzzleBoardPositions[0xE], PuzzleBoardPositions[0xD], PuzzleBoardPositions[0xC]),
            };

            base.Initialize(occupiedPositions);
        }

        /// <summary>
        /// Creates a representation of the board for display/debugging purposes.
        /// </summary>
        /// <returns>A string representation of the board.</returns>
        public override string ToString()
        {
            return string.Format(CultureInfo.CurrentCulture,
                                 "\r\n" +
                                 "        {0}\r\n" +
                                 "      {1}   {2}\r\n" +
                                 "    {3}   {4}   {5}\r\n" +
                                 "  {6}   {7}   {8}   {9}\r\n" +
                                 "{10}   {11}   {12}   {13}   {14}\r\n",
                                 PuzzleBoardPositions[0x0].Occupied ? 1 : 0,
                                 PuzzleBoardPositions[0x1].Occupied ? 1 : 0,
                                 PuzzleBoardPositions[0x2].Occupied ? 1 : 0,
                                 PuzzleBoardPositions[0x3].Occupied ? 1 : 0,
                                 PuzzleBoardPositions[0x4].Occupied ? 1 : 0,
                                 PuzzleBoardPositions[0x5].Occupied ? 1 : 0,
                                 PuzzleBoardPositions[0x6].Occupied ? 1 : 0,
                                 PuzzleBoardPositions[0x7].Occupied ? 1 : 0,
                                 PuzzleBoardPositions[0x8].Occupied ? 1 : 0,
                                 PuzzleBoardPositions[0x9].Occupied ? 1 : 0,
                                 PuzzleBoardPositions[0xA].Occupied ? 1 : 0,
                                 PuzzleBoardPositions[0xB].Occupied ? 1 : 0,
                                 PuzzleBoardPositions[0xC].Occupied ? 1 : 0,
                                 PuzzleBoardPositions[0xD].Occupied ? 1 : 0,
                                 PuzzleBoardPositions[0xE].Occupied ? 1 : 0);
        }

        #endregion

    }

}
