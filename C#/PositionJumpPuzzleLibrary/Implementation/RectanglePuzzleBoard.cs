﻿using System.Collections.Generic;
using System.Text;

namespace PuzzleSolver.PositionJumpPuzzleLibrary.Implementation
{

    // 0,0 0,1 0,2 0,3 0,4
    // 1,0 1,1 1,2 1,3 1,4
    // 2,0 2,1 2,2 2,3 2,4
    // 3,0 3,1 3,2 3,3 3,4
    // 4,0 4,1 4,2 4,3 4,4

    /// <summary>
    /// Puzzle board where the positions are arranged in a rectangle.
    /// </summary>
    class RectanglePuzzleBoard : PositionJumpPuzzleBoardBase
    {

        #region Constants

        /// <summary>
        /// Default number of columns to use when one is not specified.
        /// </summary>
        const int defaultNumberOfColumns = 5;
 
        /// <summary>
        /// Default number of rows to use when one is not specified.
        /// </summary>
        const int defaultNumberOfRows = 5;

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor using the default number of rows and columns.
        /// </summary>
        public RectanglePuzzleBoard()
            : this(defaultNumberOfRows, defaultNumberOfColumns)
        {
            // Intentionally empty
        }

        /// <summary>
        /// Constructor with the number rows and, columns specified.
        /// </summary>
        /// <param name="numberOfRows">Passed in number of rows to use.</param>
        /// <param name="numberOfColumns">Passed in number of columns to use.</param>
        public RectanglePuzzleBoard(int numberOfRows, int numberOfColumns)
            : base()
        {
            NumberOfRows = numberOfRows;
            NumberOfColumns = numberOfColumns;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Get the number of columns in puzzle board.
        /// </summary>
        public int NumberOfColumns { get; private set; }

        /// <summary>
        /// Get the number of rows in puzzle board.
        /// </summary>
        public int NumberOfRows { get; private set; }
        
        #endregion

        #region Override Properties

        /// <summary>
        /// Implementation of the number of puzzle board positions property.
        /// </summary>
        protected override int NumberOfPuzzleBoardPositions 
        {
            get { return NumberOfRows * NumberOfColumns; }
        }

        #endregion

        #region Override Methods

        /// <summary>
        /// Creates the puzzle board positions, the possible moves for each position, and sets up the initial state of the puzzle.
        /// </summary>
        /// <param name="occupiedPositions">Starting positions to use.</param>
        public override void Initialize(IList<bool> occupiedPositions)
        {
            PuzzleBoardPositions = new RowColumnPositionJumpPuzzleBoardPosition[NumberOfPuzzleBoardPositions];
            for (int row = 0; row < NumberOfRows; row++)
            {
                for (int column = 0; column < NumberOfColumns; column++)
                {
                    PuzzleBoardPositions[PositionIndex(row, column)] = new RowColumnPositionJumpPuzzleBoardPosition(row, column);
                }
            }

            List<PositionJumpPuzzleMove> possibleMoves = new List<PositionJumpPuzzleMove>();
            for (int row = 0; row < NumberOfRows; row++)
            {
                for (int column = 0; column < NumberOfColumns; column++)
                {
                    AddPossibleMoves(possibleMoves, row, column);
                }
            }
            AllPossibleMoves = possibleMoves;

            base.Initialize(occupiedPositions);
        }

        /// <summary>
        /// Creates a representation of the board for display/debugging purposes.
        /// </summary>
        /// <returns>A string representation of the board.</returns>
        public override string ToString()
        {
            StringBuilder stringBuilder = new StringBuilder();

            stringBuilder.AppendLine();
            for (int row = 0; row < NumberOfRows; row++)
            {
                stringBuilder.Append(PuzzleBoardPositions[PositionIndex(row, 0)].Occupied ? "1" : "0");
                for (int column = 1; column < NumberOfColumns; column++)
                {
                    stringBuilder.Append(" ");
                    stringBuilder.Append(PuzzleBoardPositions[PositionIndex(row, column)].Occupied ? "1" : "0");
                }
                stringBuilder.AppendLine();
            }

            return stringBuilder.ToString();
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Addss all of the possible moves for the passed in starting coordinates.
        /// </summary>
        /// <param name="possibleMovesList">List to add moves to.</param>
        /// <param name="row">Row of the position to create moves for.</param>
        /// <param name="column">Column of the position to create moves for.</param>
        void AddPossibleMoves(List<PositionJumpPuzzleMove> possibleMovesList, int row, int column)
        {
            PotentiallyAddMove(possibleMovesList, row, column, row - 1, column, row - 2, column);
            PotentiallyAddMove(possibleMovesList, row, column, row - 1, column + 1, row - 2, column + 2);
            PotentiallyAddMove(possibleMovesList, row, column, row, column + 1, row, column + 2);
            PotentiallyAddMove(possibleMovesList, row, column, row + 1, column + 1, row + 2, column + 2);
            PotentiallyAddMove(possibleMovesList, row, column, row + 1, column, row + 2, column);
            PotentiallyAddMove(possibleMovesList, row, column, row - 1, column + 1, row + 2, column - 2);
            PotentiallyAddMove(possibleMovesList, row, column, row, column - 1, row, column - 2);
            PotentiallyAddMove(possibleMovesList, row, column, row - 1, column - 1, row - 2, column - 2);
        }

        /// <summary>
        /// Returns whether the passed in row and column pair is valid on the puzzle board.
        /// </summary>
        /// <param name="row">Row to validate.</param>
        /// <param name="column">Column to validate.</param>
        /// <returns>True if the row column pair is valid in the puzzle board, false otherwise.</returns>
        bool IsPositionValid(int row, int column)
        {
            return ((0 <= row) && (row < NumberOfRows)) &&
                   ((0 <= column) && (column < NumberOfColumns));
        }

        /// <summary>
        /// Takes a row and column and returns the position index that can be used in 
        /// the base class's positions array.
        /// </summary>
        /// <param name="row">Row to calculate the position of.</param>
        /// <param name="column">Column to calculate the position of.</param>
        /// <returns>Index that can be used to accces the base class's position array.</returns>
        int PositionIndex(int row, int column)
        {
            return (row * NumberOfColumns) + column;
        }

        /// <summary>
        /// Adds a move to the list if the passed in middle and end positions are valid on the puzzle board.
        /// </summary>
        /// <param name="possibleMovesList">List to add moves to.</param>
        /// <param name="startRow">Row of the start position.</param>
        /// <param name="startColumn">Column of the start position.</param>
        /// <param name="middlePositionRow">Row of the middle position.</param>
        /// <param name="middlePositionColumn">Column of the middle position.</param>
        /// <param name="endPositionRow">Row of the end position.</param>
        /// <param name="endPositionColumn">Column of the end position.</param>
        void PotentiallyAddMove(List<PositionJumpPuzzleMove> possibleMovesList,
                                int startRow,
                                int startColumn,
                                int middlePositionRow,
                                int middlePositionColumn,
                                int endPositionRow,
                                int endPositionColumn)
        {
            if (IsPositionValid(middlePositionRow, middlePositionColumn) &&
                IsPositionValid(endPositionRow, endPositionColumn))
            {
                possibleMovesList.Add(new PositionJumpPuzzleMove(PuzzleBoardPositions[PositionIndex(startRow, startColumn)],
                                                                 PuzzleBoardPositions[PositionIndex(middlePositionRow, middlePositionColumn)],
                                                                 PuzzleBoardPositions[PositionIndex(endPositionRow, endPositionColumn)]));
            }
        }

        #endregion

    }

}
