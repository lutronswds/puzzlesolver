﻿using PuzzleSolver.Engines;

namespace PuzzleSolver.PositionJumpPuzzleLibrary.Implementation
{

    /// <summary>
    /// End of puzzle evaluator that evaluates for one-piece-in-starting-position goal.
    /// </summary>
    class OnePieceInStartingPositionEndOfPuzzleEvaluator : IEndOfPuzzleEvaluator, IStartingPositionHolder
    {

        /// <summary>
        /// Constructor with no arguments.
        /// </summary>
        public OnePieceInStartingPositionEndOfPuzzleEvaluator()
        {
            // Intentionally empty
        }

        /// <summary>
        /// Constructor with the passed in starting position to use for end of puzzle evaluation.
        /// </summary>
        /// <param name="startingPosition">Starting position to store.</param>
        public OnePieceInStartingPositionEndOfPuzzleEvaluator(IPositionJumpPuzzleBoardPosition startingPosition)
        {
            StartingPosition = startingPosition;
        }
        
        /// <summary>
        /// Set/Get the starting position to use for end of puzzle evaluation.
        /// </summary>
        public IPositionJumpPuzzleBoardPosition StartingPosition { get; set; }

        /// <summary>
        /// Evaluate function that will return if the goal has been met or not.
        /// </summary>
        /// <param name="puzzleBoard">Position-jump puzzle board to evalute.</param>
        /// <returns>A EndOfPuzzlePossiblity value that related whether the goal has been met or not.</returns>
        public EndOfPuzzlePossibility Evaluate(IPuzzleBoard puzzleBoard)
        {
            return EvaluateSolution(puzzleBoard);
        }

        /// <summary>
        /// Actual implemention of evaluate function.
        /// </summary>
        /// <param name="puzzleBoard">Position-jump puzzle board to evalute.</param>
        /// <returns>A EndOfPuzzlePossiblity value that related whether the goal has been met or not.</returns>
        EndOfPuzzlePossibility EvaluateSolution(IPuzzleBoard puzzleBoard)
        {
            IPositionJumpPuzzleBoard positionJumpPuzzleBoard = (IPositionJumpPuzzleBoard)puzzleBoard;

            if (positionJumpPuzzleBoard.GetNumberOfOccupiedPositions() == 1)
            {
                if (StartingPosition.Occupied)
                {
                    return EndOfPuzzlePossibility.Success;
                }
                else
                {
                    return EndOfPuzzlePossibility.Failure;
                }
            }
            else if (positionJumpPuzzleBoard.GetNumberOfPossibleMoves() == 0)
            {
                return EndOfPuzzlePossibility.Failure;
            }
            else
            {
                return EndOfPuzzlePossibility.KeepPlaying;
            }
        }

    }

}
