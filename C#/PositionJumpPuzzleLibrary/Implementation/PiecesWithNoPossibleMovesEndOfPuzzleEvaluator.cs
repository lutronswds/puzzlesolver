﻿using PuzzleSolver.Engines;

namespace PuzzleSolver.PositionJumpPuzzleLibrary.Implementation
{

    /// <summary>
    /// End of puzzle evaluator that evaluates for one-piece-in-starting-position goal.
    /// </summary>
    class PiecesWithNoPossibleMovesEndOfPuzzleEvaluator : IEndOfPuzzleEvaluator
    {

        /// <summary>
        /// Constructor with the passed in number of pieces left to use for end of puzzle evaluation.
        /// </summary>
        /// <param name="targetNumberOfPiecesLeft">Target number of pieces left for goal evaluation.</param>
        public PiecesWithNoPossibleMovesEndOfPuzzleEvaluator(int targetNumberOfPiecesLeft)
        {
            TargetNumberOfPiecesLeft = targetNumberOfPiecesLeft;
        }

        /// <summary>
        /// Set/get the target number of pieces left to use for end of puzzle evaluation.
        /// </summary>
        public int TargetNumberOfPiecesLeft { get; set; }

        /// <summary>
        /// Evaluate function that will return if the goal has been met or not.
        /// </summary>
        /// <param name="puzzleBoard">Position-jump puzzle board to evalute.</param>
        /// <returns>A EndOfPuzzlePossiblity value that related whether the goal has been met or not.</returns>
        public EndOfPuzzlePossibility Evaluate(IPuzzleBoard puzzleBoard)
        {
            return EvaluateSolution(puzzleBoard);
        }

        /// <summary>
        /// Actual implemention of evaluate function.
        /// </summary>
        /// <param name="puzzleBoard">Position-jump puzzle board to evalute.</param>
        /// <returns>A EndOfPuzzlePossiblity value that related whether the goal has been met or not.</returns>
        EndOfPuzzlePossibility EvaluateSolution(IPuzzleBoard puzzleBoard)
        {
            IPositionJumpPuzzleBoard positionJumpPuzzleBoard = (IPositionJumpPuzzleBoard)puzzleBoard;

            if (positionJumpPuzzleBoard.GetNumberOfOccupiedPositions() == TargetNumberOfPiecesLeft &&
                positionJumpPuzzleBoard.GetNumberOfPossibleMoves() == 0)
            {
                return EndOfPuzzlePossibility.Success;
            }
            else if (positionJumpPuzzleBoard.GetNumberOfOccupiedPositions() <= TargetNumberOfPiecesLeft)
            {
                return EndOfPuzzlePossibility.Failure;
            }
            else
            {
                return EndOfPuzzlePossibility.KeepPlaying;
            }
        }

    }

}
