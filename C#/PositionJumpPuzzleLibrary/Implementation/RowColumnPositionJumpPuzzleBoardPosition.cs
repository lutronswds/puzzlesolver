﻿using System.Globalization;

namespace PuzzleSolver.PositionJumpPuzzleLibrary.Implementation
{

    /// <summary>
    /// A position-jump puzzle board position that is uniquely identified by a row-column pair.
    /// </summary>
    class RowColumnPositionJumpPuzzleBoardPosition : IPositionJumpPuzzleBoardPosition
    {

        #region Constructor

        /// <summary>
        /// Constructor that takes in the row and column.
        /// </summary>
        /// <param name="row">Row for the position.</param>
        /// <param name="column">Column for the position.</param>
        public RowColumnPositionJumpPuzzleBoardPosition(int row, int column)
        {
            Row = row;
            Column = column;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Column for this position.
        /// </summary>
        public int Column { get; private set; }

        /// <summary>
        /// Description of the board position to be used for display purposes.
        /// </summary>
        public string Description { get { return string.Format(CultureInfo.CurrentCulture, "{0},{1}", Row, Column); } }

        /// <summary>
        /// Whether the position is currently occupied or not.
        /// </summary>
        public bool Occupied { get; set; }

        /// <summary>
        /// Row for this position.
        /// </summary>
        public int Row { get; private set; }

        #endregion

        #region Override Methods

        /// <summary>
        /// Creates a representation of the position for display/debugging purposes.
        /// </summary>
        /// <returns>A string representation of the position.</returns>
        public override string ToString()
        {
            return string.Format(CultureInfo.CurrentCulture,
                                 "{0},{1}:{2}",
                                 Row,
                                 Column,
                                 Occupied ? 1 : 0);
        }

        #endregion

    }

}
