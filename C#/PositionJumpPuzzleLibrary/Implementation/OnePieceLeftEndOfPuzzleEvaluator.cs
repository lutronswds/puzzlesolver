﻿using PuzzleSolver.Engines;

namespace PuzzleSolver.PositionJumpPuzzleLibrary.Implementation
{

    /// <summary>
    /// End of puzzle evaluator that evaluates for one-piece-left goal.
    /// </summary>
    class OnePieceLeftEndOfPuzzleEvaluator : IEndOfPuzzleEvaluator
    {

        /// <summary>
        /// Evaluate function that will return if the goal has been met or not.
        /// </summary>
        /// <param name="puzzleBoard">Position-jump puzzle board to evalute.</param>
        /// <returns>A EndOfPuzzlePossiblity value that related whether the goal has been met or not.</returns>
        public EndOfPuzzlePossibility Evaluate(IPuzzleBoard puzzleBoard)
        {
            return EvaluateSolution(puzzleBoard);
        }

        /// <summary>
        /// Actual implemention of evaluate function.
        /// </summary>
        /// <param name="puzzleBoard">Position-jump puzzle board to evalute.</param>
        /// <returns>A EndOfPuzzlePossiblity value that related whether the goal has been met or not.</returns>
        static private EndOfPuzzlePossibility EvaluateSolution(IPuzzleBoard puzzleBoard)
        {
            IPositionJumpPuzzleBoard positionJumpPuzzleBoard = (IPositionJumpPuzzleBoard)puzzleBoard;

            if (positionJumpPuzzleBoard.GetNumberOfOccupiedPositions() == 1)
            {
                return EndOfPuzzlePossibility.Success;
            }
            else if (positionJumpPuzzleBoard.GetNumberOfPossibleMoves() == 0)
            {
                return EndOfPuzzlePossibility.Failure;
            }
            else
            {
                return EndOfPuzzlePossibility.KeepPlaying;
            }
        }

    }

}
