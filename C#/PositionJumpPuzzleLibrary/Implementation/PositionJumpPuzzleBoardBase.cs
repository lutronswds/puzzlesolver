﻿using System.Collections.Generic;
//using System.Linq;

using PuzzleSolver.Engines;

namespace PuzzleSolver.PositionJumpPuzzleLibrary.Implementation
{

    /// <summary>
    /// Base class for different puzzle jump boards
    /// </summary>
    abstract class PositionJumpPuzzleBoardBase : IPositionJumpPuzzleBoard
    {

        #region Constructors

        /// <summary>
        /// Empty constructor
        /// </summary>
        protected PositionJumpPuzzleBoardBase()
        {
            // Intentionally empty
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// List of puzzle board positions.
        /// </summary>
        public IList<IPositionJumpPuzzleBoardPosition> PuzzleBoardPositions { get; protected set; }

        #endregion

        #region Public Methods

        /// <summary>
        /// Returns the number positions that are currently occupied.
        /// </summary>
        /// <returns>The number of positions currently occupied.</returns>
        public int GetNumberOfOccupiedPositions()
        {
            int numberOfPositionsLeft = 0;

            foreach (IPositionJumpPuzzleBoardPosition puzzleBoardPosition in PuzzleBoardPositions)
            {
                if (puzzleBoardPosition.Occupied)
                {
                    numberOfPositionsLeft++;
                }
            }

            return numberOfPositionsLeft;

            //return PuzzleBoardPositions.Count(puzzleBoardPosition => puzzleBoardPosition.Occupied);
        }

        /// <summary>
        /// Returns the number of currently valid moves.
        /// </summary>
        /// <returns>The number of currently valid moves.</returns>
        public int GetNumberOfPossibleMoves()
        {
            int numberOfPossibleMoves = 0;

            foreach (IPositionJumpPuzzleMove puzzleMove in AllPossibleMoves)
            {
                if (puzzleMove.IsValid)
                {
                    numberOfPossibleMoves++;
                }
            }

            return numberOfPossibleMoves;

            //return AllPossibleMoves.Count(PuzzleMove => PuzzleMove.IsValid);
        }

        /// <summary>
        /// Returns the list of all currently valid moves.
        /// </summary>
        /// <returns>An enumeration of currently valid moves.</returns>
        public IEnumerable<IPuzzleMove> GetPossibleMoves()
        {
            List<IPuzzleMove> possibleMoves = new List<IPuzzleMove>();

            foreach (IPositionJumpPuzzleMove puzzleMove in AllPossibleMoves)
            {
                if (puzzleMove.IsValid)
                {
                    possibleMoves.Add(puzzleMove);
                }
            }

            return possibleMoves;

            //return AllPossibleMoves.Where(PuzzleMove => PuzzleMove.IsValid).ToList();
        }

        /// <summary>
        /// Sets up the initial state of the puzzle.
        /// </summary>
        /// <param name="occupiedPositions">Starting positions to use.</param>
        public virtual void Initialize(IList<bool> occupiedPositions)
        {
            if (occupiedPositions == null)
            {
                for (int index = 1; index < NumberOfPuzzleBoardPositions; index++)
                {
                    PuzzleBoardPositions[index].Occupied = true;
                }
            }
            else
            {
                int positionIndex = 0;
                foreach (bool occupied in occupiedPositions)
                {
                    PuzzleBoardPositions[positionIndex].Occupied = occupied;
                    positionIndex++;
                    if (positionIndex >= NumberOfPuzzleBoardPositions)
                    {
                        break;
                    }
                }
            }
        }

        #endregion

        #region Protected Properties

        /// <summary>
        /// Enumeration of all possible moves.
        /// </summary>
        protected IEnumerable<IPositionJumpPuzzleMove> AllPossibleMoves { get; set; }

        #endregion

        #region Protected Abstract Properties

        /// <summary>
        /// Gets the number of puzzle board positions in the puzzle board.
        /// </summary>
        protected abstract int NumberOfPuzzleBoardPositions { get; }

        #endregion

    }

}
