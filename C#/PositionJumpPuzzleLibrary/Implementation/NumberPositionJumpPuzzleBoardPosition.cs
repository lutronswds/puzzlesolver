﻿using System.Globalization;

namespace PuzzleSolver.PositionJumpPuzzleLibrary.Implementation
{

    /// <summary>
    /// A position-jump puzzle board position that is uniquely identified by an index.
    /// </summary>
    class NumberPositionJumpPuzzleBoardPosition : IPositionJumpPuzzleBoardPosition
    {

        #region Constructor

        /// <summary>
        /// Constructor that takes in the position number.
        /// </summary>
        /// <param name="positionNumber">Number for this position.</param>
        public NumberPositionJumpPuzzleBoardPosition(int positionNumber)
        {
            PositionNumber = positionNumber;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Description of the board position to be used for display purposes.
        /// </summary>
        public string Description { get { return PositionNumber.ToString("X", CultureInfo.CurrentCulture); } }

        /// <summary>
        /// Whether the position is currently occupied or not.
        /// </summary>
        public bool Occupied { get; set; }

        /// <summary>
        /// Number for this position.
        /// </summary>
        public int PositionNumber { get; private set; }

        #endregion

        #region Override Methods

        /// <summary>
        /// Creates a representation of the position for display/debugging purposes.
        /// </summary>
        /// <returns>A string representation of the position.</returns>
        public override string ToString()
        {
            return string.Format(CultureInfo.CurrentCulture,
                                 "{0:X}:{1}",
                                 PositionNumber,
                                 Occupied ? 1 : 0);
        }

        #endregion

    }

}
