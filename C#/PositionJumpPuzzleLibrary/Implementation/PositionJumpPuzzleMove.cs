﻿using System.Globalization;

namespace PuzzleSolver.PositionJumpPuzzleLibrary.Implementation
{

    /// <summary>
    /// Position-jump puzzle move.
    /// </summary>
    class PositionJumpPuzzleMove : IPositionJumpPuzzleMove
    {

        #region Constructor

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="startPosition">The starting position of the move (i.e. the starting position for the peg).</param>
        /// <param name="middlePosition">The middle position of the move (i.e. the position "jumped" over).</param>
        /// <param name="endPosition">The end position of the move (i.e. the landing position for the peg).</param>
        public PositionJumpPuzzleMove(IPositionJumpPuzzleBoardPosition startPosition, 
                                      IPositionJumpPuzzleBoardPosition middlePosition, 
                                      IPositionJumpPuzzleBoardPosition endPosition)
        {
            StartPosition = startPosition;
            MiddlePosition = middlePosition;
            EndPosition = endPosition;
        }

        #endregion

        #region Properties

        /// <summary>
        /// The end position of the move (i.e. the landing position for the peg).
        /// </summary>
        public IPositionJumpPuzzleBoardPosition EndPosition { get; set; }

        /// <summary>
        /// Returns whether or not the move is valid based on the status of the positions involved.
        /// </summary>
        public bool IsValid
        {
            get
            {
                return StartPosition.Occupied &&
                       MiddlePosition.Occupied &&
                       !EndPosition.Occupied;
            }
        }

        /// <summary>
        /// The middle position of the move (i.e. the position "jumped" over).
        /// </summary>
        public IPositionJumpPuzzleBoardPosition MiddlePosition { get; set; }

        /// <summary>
        /// The starting position of the move (i.e. the starting position for the peg).
        /// </summary>
        public IPositionJumpPuzzleBoardPosition StartPosition { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Execute the move which will change the the state of the puzzle board.
        /// </summary>
        public void Execute()
        {
            StartPosition.Occupied = false;
            MiddlePosition.Occupied = false;
            EndPosition.Occupied = true;
        }

        /// <summary>
        /// Undo the execution of the move which will restore the state of the puzzle board.
        /// </summary>
        public void Undo()
        {
            EndPosition.Occupied = false;
            MiddlePosition.Occupied = true;
            StartPosition.Occupied = true;
        }

        #endregion

        #region Override Methods

        /// <summary>
        /// Creates a representation of the move for display/debugging purposes.
        /// </summary>
        /// <returns>A string representation of the move.</returns>
        public override string ToString()
        {
            return string.Format(CultureInfo.CurrentCulture, "[{0} -> {1}]", StartPosition.Description, EndPosition.Description);
        }

        #endregion

    }

}
