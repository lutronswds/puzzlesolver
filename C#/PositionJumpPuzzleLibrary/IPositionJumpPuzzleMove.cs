﻿using PuzzleSolver.Engines;

namespace PuzzleSolver.PositionJumpPuzzleLibrary
{

    /// <summary>
    /// Interface for classes that represent a move in a position-jump puzzle.
    /// Any move in a position-jump game has a start position, a middle position, and an end position. A move
    /// involves making the start and middle positions unoccupied and the end position occupied.
    /// </summary>
    public interface IPositionJumpPuzzleMove : IPuzzleMove
    {

        /// <summary>
        /// The end position of the move (i.e. the landing position for the peg).
        /// </summary>
        IPositionJumpPuzzleBoardPosition EndPosition { get; }

        /// <summary>
        /// Returns whether or not the move is valid based on the status of the positions involved.
        /// </summary>
        bool IsValid { get; }

        /// <summary>
        /// The middle position of the move (i.e. the position "jumped" over).
        /// </summary>
        IPositionJumpPuzzleBoardPosition MiddlePosition { get; }

        /// <summary>
        /// The starting position of the move (i.e. the starting position for the peg).
        /// </summary>
        IPositionJumpPuzzleBoardPosition StartPosition { get; }

    }

}
