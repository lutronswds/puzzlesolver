﻿using System.Collections.Generic;

using PuzzleSolver.Engines;

namespace PuzzleSolver.PositionJumpPuzzleLibrary
{

    /// <summary>
    /// Interface for classes that hold puzzle state for position-jump puzzles.
    /// </summary>
    public interface IPositionJumpPuzzleBoard : IPuzzleBoard
    {

        /// <summary>
        /// All of the positions in the puzzle.
        /// </summary>
        IList<IPositionJumpPuzzleBoardPosition> PuzzleBoardPositions { get; }

        /// <summary>
        /// Returns the number of pieces left on the puzzle board.
        /// </summary>
        /// <returns>Number of pieces left.</returns>
        int GetNumberOfOccupiedPositions();

        /// <summary>
        /// Returns how many possibles moves there currently are based on the state of puzzle board.
        /// </summary>
        /// <returns>How many possible moves there are.</returns>
        int GetNumberOfPossibleMoves();

    }

}
