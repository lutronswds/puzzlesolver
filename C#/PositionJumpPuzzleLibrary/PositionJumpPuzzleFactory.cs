﻿using System.Collections.Generic;

using PuzzleSolver.Engines;
using PuzzleSolver.PositionJumpPuzzleLibrary.Implementation;

namespace PuzzleSolver.PositionJumpPuzzleLibrary
{

    /// <summary>
    /// Factory class to create objects in the Position-Jump Puzzle libarary on demand.
    /// </summary>
    public static class PositionJumpPuzzleFactory
    {

        #region CreateOnePieceInStartingPositionEndOfPuzzleEvaluator

        /// <summary>
        /// Creates a one-piece-in-starting-position end of puzzle evalator with no specified starting position.
        /// </summary>
        /// <returns>An object the implements the IEndOfPuzzleEvaluator interface and checks for a single piece in the starting position.</returns>
        public static IEndOfPuzzleEvaluator CreateOnePieceInStartingPositionEndOfPuzzleEvaluator()
        {
            return new OnePieceInStartingPositionEndOfPuzzleEvaluator();
        }

        /// <summary>
        /// Creates a one-piece-in-starting-position end of puzzle evalator with a specified starting position.
        /// </summary>
        /// <param name="startingPosition">Position that is empty that the final move should fill.</param>
        /// <returns>An object the implements the IEndOfPuzzleEvaluator interface and checks for a single piece in the starting position.</returns>
        public static IEndOfPuzzleEvaluator CreateOnePieceInStartingPositionEndOfPuzzleEvaluator(IPositionJumpPuzzleBoardPosition startingPosition)
        {
            return new OnePieceInStartingPositionEndOfPuzzleEvaluator(startingPosition);
        }

        #endregion

        /// <summary>
        /// Creates a one-piece-left end of puzzle evalator.
        /// </summary>
        /// <returns>An object the implements the IEndOfPuzzleEvaluator interface and checks for a single piece left.</returns>
        public static IEndOfPuzzleEvaluator CreateOnePieceLeftEndOfPuzzleEvaluator()
        {
            return new OnePieceLeftEndOfPuzzleEvaluator();
        }

        /// <summary>
        /// Creates a specified-number-of-pieces-with-no-moves-left end of puzzle evalator.
        /// </summary>
        /// <param name="targetNumberOfPiecesLeft">Number of pieces left to use as a end of puzzle goal.</param>
        /// <returns>An object the implements the IEndOfPuzzleEvaluator interface and checks for the specified number of pieces left with no possible moves.</returns>
        public static IEndOfPuzzleEvaluator CreatePiecesWithNoPossibleMovesEndOfPuzzleEvaluator(int targetNumberOfPiecesLeft)
        {
            return new PiecesWithNoPossibleMovesEndOfPuzzleEvaluator(targetNumberOfPiecesLeft);
        }

        #region CreateTrianglePuzzleBoard

        /// <summary>
        /// Creates a triangle puzzle board with the default configuration.
        /// </summary>
        /// <returns>Triangle puzzle board with default configuration.</returns>
        public static IPositionJumpPuzzleBoard CreateTrianglePuzzleBoard()
        {
            TrianglePuzzleBoard trianglePuzzleBoard = new TrianglePuzzleBoard();

            trianglePuzzleBoard.Initialize(null);

            return trianglePuzzleBoard;
        }

        /// <summary>
        /// Creates a triangle puzzle board with the passed in starting positions.
        /// </summary>
        /// <param name="startingPositions">Which positions are occupied.</param>
        /// <returns>Triangle puzzle board with the passed in initial settings.</returns>
        public static IPositionJumpPuzzleBoard CreateTrianglePuzzleBoard(IList<bool> startingPositions)
        {
            TrianglePuzzleBoard trianglePuzzleBoard = new TrianglePuzzleBoard();

            trianglePuzzleBoard.Initialize(startingPositions);

            return trianglePuzzleBoard;
        }

        #endregion

    }

}
