﻿namespace PuzzleSolver.PositionJumpPuzzleLibrary
{

    /// <summary>
    /// Interface for any class that supports a starting position property that can be set publically.
    /// </summary>
    public interface IStartingPositionHolder
    {

        /// <summary>
        /// The starting position that has been set previously.
        /// </summary>
        IPositionJumpPuzzleBoardPosition StartingPosition { get; set; }

    }

}
