

#include <iostream>
#include <list>
#include <string>

#include "IPositionJumpPuzzleBoard.h"
#include "IFirstSolutionPuzzleSolverEngine.h"
#include "PositionJumpPuzzleFactory.h"
#include "PuzzleSolverEngineFactory.h"
#include "IPuzzleMove.h"

struct Configuration
{
	IEndOfPuzzleEvaluator * evaluator;
	int startPositionIndex;
	std::list<bool> boardConfig;

	Configuration() :
		evaluator(NULL),
		startPositionIndex(0),
		boardConfig() {}

	~Configuration()
	{
		// strictly speaking, this isn't great design.
		// evaluator could theoretically not be on the 
		// heap at all.
		if (evaluator)
		{
			delete evaluator;
		}
	}
};

void ParseConfigOptions(struct Configuration * config, int argc, char const *argv[]);

int main(int argc, char const *argv[])
{
	struct Configuration config;

	std::cout << "Starting configuration:\n";
	ParseConfigOptions(&config, argc, argv);

	IPositionJumpPuzzleBoard * puzzleBoard = PositionJumpPuzzleFactory::CreateTrianglePuzzleBoard(&config.boardConfig);

	IFirstSolutionPuzzleSolverEngine * engine = PuzzleSolverEngineFactory::CreateFirstSolutionPuzzleSolverEngine();

	// now run the puzzle
	std::list<IPuzzleMove *> moves;
	engine->Execute(puzzleBoard, config.evaluator, &moves);

	if (moves.empty())
	{
		std::cout << "No solution found :(\n";
	}
	else
	{
		// display all moves
		for (std::list<IPuzzleMove *>::iterator i = moves.begin(); i != moves.end(); ++i)
		{
			IPuzzleMove * move = *i;
			std::cout << move->ToString() << std::endl;
		}	
	}

	// free my memory, UNLIKE THOSE DIRTY C# HEATHENS.
	// kidding.  <3 Matt.
	delete engine;
	delete puzzleBoard;

	return 0;
}

void ParseConfigOptions(struct Configuration * config, int argc, char const *argv[])
{
	for (int argIndex = 0; argIndex < argc; ++argIndex)
	{
		std::string arg(argv[argIndex]);

		// parse arguments here
		// for now handle -s option
		if (arg.find("-s") != std::string::npos)
		{
			// extract just the 1's and 0's
			std::string configString = arg.substr(2);

			config->startPositionIndex = -1;
			int index = 0;

			for (std::string::iterator i = configString.begin(); i != configString.end(); ++i)
			{
				char c = *i;
				if (c=='0')
				{
					config->boardConfig.push_back(false);
					if (config->startPositionIndex == -1)
					{
						config->startPositionIndex = index;
					}
					else
					{
						std::cerr << "Too many empty positions.  Using defaults.\n";
						config->boardConfig.clear();
						break;
					}
				}
				else if (c=='1')
				{
					config->boardConfig.push_back(true);
				}
				else
				{
					std::cerr << "Invalid configuration string.  Using defaults\n";
					config->boardConfig.clear();
					config->startPositionIndex = 0;
					break;
				}

				++index;
			}
		}
	}

	// default to one piece left evaluator
	if (config->evaluator == NULL)
	{
		config->evaluator = PositionJumpPuzzleFactory::CreateOnePieceLeftEndOfPuzzleEvaluator();
	}
}