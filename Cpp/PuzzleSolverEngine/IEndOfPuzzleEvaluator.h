#ifndef _IENDOFPUZZLEEVALUATOR
#define _IENDOFPUZZLEEVALUATOR

#include "IPuzzleBoard.h"

typedef enum
{
	Unknown,
	KeepPlaying,
	Success,
	Failure
} EndOfPuzzlePossibility;

class IEndOfPuzzleEvaluator
{
	public:
		virtual ~IEndOfPuzzleEvaluator() {};

		virtual EndOfPuzzlePossibility Evaluate(IPuzzleBoard *puzzleBoard) = 0;
};

#endif