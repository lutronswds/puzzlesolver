#ifndef _IFIRSTSOLUTIONPUZZLESOLVERENGINE_H
#define _IFIRSTSOLUTIONPUZZLESOLVERENGINE_H

#include <list>
#include "IPuzzleBoard.h"
#include "IEndOfPuzzleEvaluator.h"

class IFirstSolutionPuzzleSolverEngine
{
public:
	
	virtual ~IFirstSolutionPuzzleSolverEngine() {};

	virtual bool Execute (IPuzzleBoard *puzzleBoard,
						IEndOfPuzzleEvaluator *endOfPuzzleEvaluator,
						std::list<IPuzzleMove*> *successfulMoves) = 0;
};


#endif