#ifndef _IPUZZLEMOVE_H
#define _IPUZZLEMOVE_H

#include <string>

class IPuzzleMove
{
	public:
		virtual ~IPuzzleMove() {};

		virtual void Execute() = 0;
		virtual void Undo() = 0;

		virtual std::string ToString() = 0;
};

#endif