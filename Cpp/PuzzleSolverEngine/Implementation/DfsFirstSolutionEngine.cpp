#include "DfsFirstSolutionEngine.h"

bool DfsFirstSolutionEngine::Execute(IPuzzleBoard *puzzleBoard,
										   IEndOfPuzzleEvaluator *endOfPuzzleEvaluator,
										   std::list<IPuzzleMove *> *successfulMoves)
{
	return ExecuteRecursively(puzzleBoard, endOfPuzzleEvaluator, successfulMoves);	
}

bool DfsFirstSolutionEngine::ExecuteRecursively(IPuzzleBoard *puzzleBoard,
									   IEndOfPuzzleEvaluator *endOfPuzzleEvaluator,
									   std::list<IPuzzleMove*> *successfulMoves)
{
	switch (endOfPuzzleEvaluator->Evaluate(puzzleBoard))
	{
		case Failure:
			return false;

		case Success:
			return true;

		default:
			std::list<IPuzzleMove *> possibleMoves;
			puzzleBoard->GetPossibleMoves(&possibleMoves);
			// if you haven't seen this before, it's because it's new for C++ 11.
			// it just iterates through the data structure on the right and puts the
			// value in the data structure on the left.  this nicely reduces boilerplate
			// code and is way easier to read.
			for (IPuzzleMove *puzzleMove : possibleMoves)
			{
				puzzleMove->Execute();
				if (ExecuteRecursively(puzzleBoard, endOfPuzzleEvaluator, successfulMoves))
				{
					// Moves are always added to the fron of the list because adding to the end
					// would result in the list in the reverse order.
					successfulMoves->push_front(puzzleMove);
					return true;
				}
				else
				{
					// Undo the move so that the board is back in its initial state before
					// trying the next move.
					puzzleMove->Undo();
				}
			}
			return false;
	}
}