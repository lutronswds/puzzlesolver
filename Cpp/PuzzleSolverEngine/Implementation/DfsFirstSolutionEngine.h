#ifndef _DFSFIRSTSOLUTIONENGINE_H
#define _DFSFIRSTSOLUTIONENGINE_H

#include <list>
#include "../IFirstSolutionPuzzleSolverEngine.h"

class DfsFirstSolutionEngine : public IFirstSolutionPuzzleSolverEngine
{
public:
	DfsFirstSolutionEngine() {};
	virtual ~DfsFirstSolutionEngine() {};

	virtual bool Execute(IPuzzleBoard *puzzleBoard,
						   IEndOfPuzzleEvaluator *endOfPuzzleEvaluator,
						   std::list<IPuzzleMove *> *successfulMoves);

private:	
	bool ExecuteRecursively(IPuzzleBoard *puzzleBoard,
							IEndOfPuzzleEvaluator *endOfPuzzleEvalutator,
							std::list<IPuzzleMove *> *successfulMoves);
};


#endif