
#include "PuzzleSolverEngineFactory.h"
#include "DfsFirstSolutionEngine.h"

IFirstSolutionPuzzleSolverEngine * PuzzleSolverEngineFactory::CreateFirstSolutionPuzzleSolverEngine()
{
	return new DfsFirstSolutionEngine();
}
