#ifndef _IPUZZLEBOARD_H
#define _IPUZZLEBOARD_H 

#include <list>

#include "IPuzzleMove.h"

class IPuzzleBoard
{
	public:
		virtual ~IPuzzleBoard() {};
		
		virtual void GetPossibleMoves(std::list<IPuzzleMove *> * moveList) = 0;
		// There isn't a handy replacement for IEnumerable in C++. I'll just use lists for everything
};

#endif