#ifndef _PUZZLESOLVERENGINEFACTORY_H
#define _PUZZLESOLVERENGINEFACTORY_H

#include "IFirstSolutionPuzzleSolverEngine.h"

class PuzzleSolverEngineFactory
{
public:
	static IFirstSolutionPuzzleSolverEngine * CreateFirstSolutionPuzzleSolverEngine();
};

#endif