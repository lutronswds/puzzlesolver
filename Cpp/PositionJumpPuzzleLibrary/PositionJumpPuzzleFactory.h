#ifndef _POSITIONJUMPPUZZLEFACTORY_H
#define _POSITIONJUMPPUZZLEFACTORY_H

#include <list>

#include "IEndOfPuzzleEvaluator.h"
#include "IPositionJumpPuzzleBoard.h"
class PositionJumpPuzzleFactory
{
public:
	// Puzzle boards
	static IPositionJumpPuzzleBoard * CreateTrianglePuzzleBoard(std::list<bool> * startingConfig);

	static IEndOfPuzzleEvaluator * CreateOnePieceLeftEndOfPuzzleEvaluator();
};

#endif