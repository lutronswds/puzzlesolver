#ifndef _ISTARTINGPOSITIONHOLDER_H
#define _ISTARTINGPOSITIONHOLDER_H

#include "IPositionJumpPuzzleBoardPosition.h"

class IStartingPositionHolder
{
public:
	virtual ~IStartingPositionHolder() {};

	virtual IPositionJumpPuzzleBoardPosition StartingPosition() = 0;
	virtual void SetStartingPosition(IPositionJumpPuzzleBoardPosition *newStart) = 0;

};



#endif