#ifndef _IPOSITIONJUMPPUZZLEBOARD_H
#define _IPOSITIONJUMPPUZZLEBOARD_H

#include <vector>

#include "IPositionJumpPuzzleBoardPosition.h"
#include "IPuzzleBoard.h"

class IPositionJumpPuzzleBoard : public IPuzzleBoard
{
public:
	virtual ~IPositionJumpPuzzleBoard() {};

	virtual std::vector<IPositionJumpPuzzleBoardPosition *> * PuzzleBoardPositions() = 0;
	virtual int GetNumberOfOccupiedPositions() = 0;
	virtual int GetNumberOfPossibleMoves() = 0;

};

#endif