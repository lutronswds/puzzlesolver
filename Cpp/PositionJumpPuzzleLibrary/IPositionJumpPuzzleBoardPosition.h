#ifndef _IPOSITIONJUMPPUZZLEBOARDPOSITION_H
#define _IPOSITIONJUMPPUZZLEBOARDPOSITION_H

#include <string>

class IPositionJumpPuzzleBoardPosition
{
public:
	virtual ~IPositionJumpPuzzleBoardPosition() {}
	virtual std::string Description() = 0;

	virtual bool Occupied() = 0;
	virtual void SetOccupied(bool occupied) = 0;

};

#endif