
#include "PositionJumpPuzzleFactory.h"

#include "OnePieceLeftEndOfPuzzleEvaluator.h"
#include "TrianglePuzzleBoard.h"

IPositionJumpPuzzleBoard * PositionJumpPuzzleFactory::CreateTrianglePuzzleBoard(std::list<bool> * startingConfig)
{
	TrianglePuzzleBoard * board = new TrianglePuzzleBoard();
	board->Initialize(startingConfig);
	return board;
}

IEndOfPuzzleEvaluator * PositionJumpPuzzleFactory::CreateOnePieceLeftEndOfPuzzleEvaluator()
{
	return new OnePieceLeftEndOfPuzzleEvaluator();
}
