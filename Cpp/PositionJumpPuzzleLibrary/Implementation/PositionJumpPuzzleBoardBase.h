#ifndef _POSITIONJUMPPUZZLEBOARDBASE_H
#define _POSITIONJUMPPUZZLEBOARDBASE_H

#include "IPositionJumpPuzzleBoard.h"
#include "IPositionJumpPuzzleBoardPosition.h"
#include "IPositionJumpPuzzleMove.h"

class PositionJumpPuzzleBoardBase : public IPositionJumpPuzzleBoard
{
public:
	virtual ~PositionJumpPuzzleBoardBase();
	virtual std::vector<IPositionJumpPuzzleBoardPosition *> * PuzzleBoardPositions();

	virtual int GetNumberOfOccupiedPositions();
	virtual int GetNumberOfPossibleMoves();
	virtual void GetPossibleMoves(std::list<IPuzzleMove *> *moveList);
	virtual int NumberOfPuzzleBoardPositions() = 0;

protected:
	PositionJumpPuzzleBoardBase() {};

	void ConfigureBoard(std::list<bool> *occupiedPositions);

	virtual void Initialize(std::list<bool> *occupiedPositions) = 0;

	std::list<IPositionJumpPuzzleMove *> allPossibleMoves_;
	std::vector<IPositionJumpPuzzleBoardPosition *> puzzleBoardPositions_;
		
};

#endif