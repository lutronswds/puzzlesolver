#include "PositionJumpPuzzleMove.h"


PositionJumpPuzzleMove::PositionJumpPuzzleMove(IPositionJumpPuzzleBoardPosition * startPosition,
						IPositionJumpPuzzleBoardPosition * middlePosition,
						IPositionJumpPuzzleBoardPosition * endPosition) :
	startPosition_(startPosition),
	middlePosition_(middlePosition),
	endPosition_(endPosition)
{}

IPositionJumpPuzzleBoardPosition * PositionJumpPuzzleMove::StartPosition()
{
	return startPosition_;	
}

IPositionJumpPuzzleBoardPosition * PositionJumpPuzzleMove::MiddlePosition()
{
	return middlePosition_;	
}

IPositionJumpPuzzleBoardPosition * PositionJumpPuzzleMove::EndPosition()
{
	return endPosition_;
}

bool PositionJumpPuzzleMove::IsValid()
{
	return startPosition_->Occupied() && middlePosition_->Occupied()
			&& ! endPosition_->Occupied();
}

void PositionJumpPuzzleMove::Execute()
{
	startPosition_->SetOccupied(false);
	middlePosition_->SetOccupied(false);
	endPosition_->SetOccupied(true);
}

void PositionJumpPuzzleMove::Undo()
{
	startPosition_->SetOccupied(true);
	middlePosition_->SetOccupied(true);
	endPosition_->SetOccupied(false);
}

std::string PositionJumpPuzzleMove::ToString()
{
	return startPosition_->Description() + " -> " + endPosition_->Description();
}