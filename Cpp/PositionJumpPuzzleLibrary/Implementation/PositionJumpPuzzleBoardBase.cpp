#include "PositionJumpPuzzleBoardBase.h"

PositionJumpPuzzleBoardBase::~PositionJumpPuzzleBoardBase()
{
	// free all the moves
	for (std::list<IPositionJumpPuzzleMove *>::iterator i = allPossibleMoves_.begin(); i != allPossibleMoves_.end(); ++i)
	{
		IPositionJumpPuzzleMove * move = *i;
		delete move;
	}

	// free all the board positions
	for (std::vector<IPositionJumpPuzzleBoardPosition *>::iterator i = puzzleBoardPositions_.begin(); i != puzzleBoardPositions_.end(); ++i)
	{
		IPositionJumpPuzzleBoardPosition * position = *i;
		delete position;
	}
}

void PositionJumpPuzzleBoardBase::ConfigureBoard(std::list<bool> *occupiedPositions)
{
	if (occupiedPositions == NULL)
	{
		for (std::vector<IPositionJumpPuzzleBoardPosition *>::iterator i = puzzleBoardPositions_.begin(); i != puzzleBoardPositions_.end(); ++i)
		{
			IPositionJumpPuzzleBoardPosition *position = *i;
			position->SetOccupied(true);
		}
	}
	else
	{
		int positionIndex = 0;
		for (std::list<bool>::iterator i = occupiedPositions->begin(); i != occupiedPositions->end(); ++i)
		{
			bool occupied = *i;
			puzzleBoardPositions_[positionIndex]->SetOccupied(occupied);
			positionIndex++;

			if (positionIndex >= NumberOfPuzzleBoardPositions())
			{
				break;	
			}
		}
	}
}

std::vector<IPositionJumpPuzzleBoardPosition *> * PositionJumpPuzzleBoardBase::PuzzleBoardPositions()
{
	return &puzzleBoardPositions_;
}

int PositionJumpPuzzleBoardBase::GetNumberOfOccupiedPositions()
{
	int numPositionsLeft = 0;

	for (std::vector<IPositionJumpPuzzleBoardPosition *>::iterator i = puzzleBoardPositions_.begin(); i != puzzleBoardPositions_.end(); ++i)
	{
		IPositionJumpPuzzleBoardPosition *position = *i;
		if (position->Occupied())
		{
			numPositionsLeft++;
		}
	}

	return numPositionsLeft;
}

int PositionJumpPuzzleBoardBase::GetNumberOfPossibleMoves()
{
	int numPossibleMoves = 0;

	for (std::list<IPositionJumpPuzzleMove *>::iterator i = allPossibleMoves_.begin(); i != allPossibleMoves_.end(); ++i)
	{
		IPositionJumpPuzzleMove * move = *i;
		if (move->IsValid())
		{
			numPossibleMoves++;
		}
	}

	return numPossibleMoves;
}

void PositionJumpPuzzleBoardBase::GetPossibleMoves(std::list<IPuzzleMove *> *moveList)
{
	for (std::list<IPositionJumpPuzzleMove *>::iterator i = allPossibleMoves_.begin(); i != allPossibleMoves_.end(); ++i)
	{
		IPositionJumpPuzzleMove *move = *i;
		if (move->IsValid())
		{
			moveList->push_back(move);
		}
	}
}