#ifndef _ROWCOLUMNPOSITIONJUMPPUZZLEBOARDPOSITION
#define _ROWCOLUMNPOSITIONJUMPPUZZLEBOARDPOSITION

#include "../IPositionJumpPuzzleBoardPosition.h"

class RowColumnPositionJumpPuzzleBoardPosition : public IPositionJumpPuzzleBoardPosition
{
public:
	virtual ~RowColumnPositionJumpPuzzleBoardPosition() {};

	RowColumnPositionJumpPuzzleBoardPosition(int row, int column):
		row_(row),
		column_(column),
		occupied_(false) {}

	virtual bool Occupied() { return occupied_; }
	virtual void SetOccupied(bool isOccupied) { occupied_ = isOccupied; }

	// TODO: fill this out.. probably don't need to make a .cpp file
	virtual std::string Description();

	int Column() { return column_; }
	int Row() { return row_; }

private:
	int row_;
	int column_;
	bool occupied_;
};

#endif