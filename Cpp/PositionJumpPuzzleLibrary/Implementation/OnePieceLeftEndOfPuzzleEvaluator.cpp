#include "OnePieceLeftEndOfPuzzleEvaluator.h"
#include "IPositionJumpPuzzleBoard.h"


EndOfPuzzlePossibility OnePieceLeftEndOfPuzzleEvaluator::Evaluate(IPuzzleBoard * puzzleBoard)
{
	return EvaluateSolution(puzzleBoard);
}

EndOfPuzzlePossibility OnePieceLeftEndOfPuzzleEvaluator::EvaluateSolution(IPuzzleBoard * puzzleboard)
{
	IPositionJumpPuzzleBoard * jumpPuzzleBoard = dynamic_cast<IPositionJumpPuzzleBoard *>(puzzleboard);

	if (jumpPuzzleBoard->GetNumberOfOccupiedPositions() == 1)
	{
		return Success;
	}
	else if (jumpPuzzleBoard->GetNumberOfPossibleMoves() == 0)
	{
		return Failure;
	}
	else
	{
		return KeepPlaying;
	}
}
