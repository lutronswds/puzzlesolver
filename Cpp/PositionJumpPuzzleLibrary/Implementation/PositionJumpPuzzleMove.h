#ifndef _POSITIONJUMPPUZZLEMOVE_H
#define _POSITIONJUMPPUZZLEMOVE_H

#include "../IPositionJumpPuzzleMove.h"
#include "IPositionJumpPuzzleBoardPosition.h"

class PositionJumpPuzzleMove : public IPositionJumpPuzzleMove
{
public:
	virtual ~PositionJumpPuzzleMove() {};

	PositionJumpPuzzleMove(IPositionJumpPuzzleBoardPosition * startPosition,
							IPositionJumpPuzzleBoardPosition * middlePosition,
							IPositionJumpPuzzleBoardPosition * endPosition);

	virtual IPositionJumpPuzzleBoardPosition * StartPosition();
	virtual IPositionJumpPuzzleBoardPosition * MiddlePosition();
	virtual IPositionJumpPuzzleBoardPosition * EndPosition();
	
	virtual bool IsValid();

	virtual void Execute();

	virtual void Undo();

	virtual std::string ToString();

private:
	IPositionJumpPuzzleBoardPosition * startPosition_;
	IPositionJumpPuzzleBoardPosition * middlePosition_;
	IPositionJumpPuzzleBoardPosition * endPosition_;

};


#endif