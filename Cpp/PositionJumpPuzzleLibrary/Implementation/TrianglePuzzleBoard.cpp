#include "TrianglePuzzleBoard.h"

#include "NumberPositionJumpPuzzleBoardPosition.h"
#include "PositionJumpPuzzleMove.h"


void TrianglePuzzleBoard::Initialize(std::list<bool> *occupiedPositions)
{
	puzzleBoardPositions_.resize(NUM_POSITIONS);

	// create the board positions
	for (int index = 0; index < NUM_POSITIONS; ++index)
	{
		puzzleBoardPositions_[index] = new NumberPositionJumpPuzzleBoardPosition(index);	
	}

	PositionJumpPuzzleMove * all_moves[] =
	{	
		// starting from position 0
		new PositionJumpPuzzleMove(puzzleBoardPositions_[0x0], puzzleBoardPositions_[0x1], puzzleBoardPositions_[0x3]),
		new PositionJumpPuzzleMove(puzzleBoardPositions_[0x0], puzzleBoardPositions_[0x2], puzzleBoardPositions_[0x5]),
		// starting from position 1
		new PositionJumpPuzzleMove(puzzleBoardPositions_[0x1], puzzleBoardPositions_[0x3], puzzleBoardPositions_[0x6]),
		new PositionJumpPuzzleMove(puzzleBoardPositions_[0x1], puzzleBoardPositions_[0x4], puzzleBoardPositions_[0x8]),
		// starting from position 2
		new PositionJumpPuzzleMove(puzzleBoardPositions_[0x2], puzzleBoardPositions_[0x4], puzzleBoardPositions_[0x7]),
		new PositionJumpPuzzleMove(puzzleBoardPositions_[0x2], puzzleBoardPositions_[0x5], puzzleBoardPositions_[0x9]),
		// starting from position 3
		new PositionJumpPuzzleMove(puzzleBoardPositions_[0x3], puzzleBoardPositions_[0x1], puzzleBoardPositions_[0x0]),
		new PositionJumpPuzzleMove(puzzleBoardPositions_[0x3], puzzleBoardPositions_[0x4], puzzleBoardPositions_[0x5]),
		new PositionJumpPuzzleMove(puzzleBoardPositions_[0x3], puzzleBoardPositions_[0x6], puzzleBoardPositions_[0xA]),
		new PositionJumpPuzzleMove(puzzleBoardPositions_[0x3], puzzleBoardPositions_[0x7], puzzleBoardPositions_[0xC]),
		// starting from position 4
		new PositionJumpPuzzleMove(puzzleBoardPositions_[0x4], puzzleBoardPositions_[0x7], puzzleBoardPositions_[0xB]),
		new PositionJumpPuzzleMove(puzzleBoardPositions_[0x4], puzzleBoardPositions_[0x8], puzzleBoardPositions_[0xD]),
		// starting from position 5
		new PositionJumpPuzzleMove(puzzleBoardPositions_[0x5], puzzleBoardPositions_[0x2], puzzleBoardPositions_[0x0]),
		new PositionJumpPuzzleMove(puzzleBoardPositions_[0x5], puzzleBoardPositions_[0x4], puzzleBoardPositions_[0x3]),
		new PositionJumpPuzzleMove(puzzleBoardPositions_[0x5], puzzleBoardPositions_[0x8], puzzleBoardPositions_[0xC]),
		new PositionJumpPuzzleMove(puzzleBoardPositions_[0x5], puzzleBoardPositions_[0x9], puzzleBoardPositions_[0xE]),
		// starting from position 6
		new PositionJumpPuzzleMove(puzzleBoardPositions_[0x6], puzzleBoardPositions_[0x3], puzzleBoardPositions_[0x1]),
		new PositionJumpPuzzleMove(puzzleBoardPositions_[0x6], puzzleBoardPositions_[0x7], puzzleBoardPositions_[0x8]),
		// starting from position 7
		new PositionJumpPuzzleMove(puzzleBoardPositions_[0x7], puzzleBoardPositions_[0x4], puzzleBoardPositions_[0x2]),
		new PositionJumpPuzzleMove(puzzleBoardPositions_[0x7], puzzleBoardPositions_[0x8], puzzleBoardPositions_[0x9]),
		// starting from position 8
		new PositionJumpPuzzleMove(puzzleBoardPositions_[0x8], puzzleBoardPositions_[0x4], puzzleBoardPositions_[0x1]),
		new PositionJumpPuzzleMove(puzzleBoardPositions_[0x8], puzzleBoardPositions_[0x7], puzzleBoardPositions_[0x6]),
		// starting from position 9
		new PositionJumpPuzzleMove(puzzleBoardPositions_[0x9], puzzleBoardPositions_[0x5], puzzleBoardPositions_[0x2]),
		new PositionJumpPuzzleMove(puzzleBoardPositions_[0x9], puzzleBoardPositions_[0x8], puzzleBoardPositions_[0x7]),
		// starting from position A
		new PositionJumpPuzzleMove(puzzleBoardPositions_[0xA], puzzleBoardPositions_[0x6], puzzleBoardPositions_[0x3]),
		new PositionJumpPuzzleMove(puzzleBoardPositions_[0xA], puzzleBoardPositions_[0xB], puzzleBoardPositions_[0xC]),
		// starting from position B
		new PositionJumpPuzzleMove(puzzleBoardPositions_[0xB], puzzleBoardPositions_[0x7], puzzleBoardPositions_[0x4]),
		new PositionJumpPuzzleMove(puzzleBoardPositions_[0xB], puzzleBoardPositions_[0xC], puzzleBoardPositions_[0xD]),
		// starting from position C
		new PositionJumpPuzzleMove(puzzleBoardPositions_[0xC], puzzleBoardPositions_[0x7], puzzleBoardPositions_[0x3]),
		new PositionJumpPuzzleMove(puzzleBoardPositions_[0xC], puzzleBoardPositions_[0x8], puzzleBoardPositions_[0x5]),
		new PositionJumpPuzzleMove(puzzleBoardPositions_[0xC], puzzleBoardPositions_[0xB], puzzleBoardPositions_[0xA]),
		new PositionJumpPuzzleMove(puzzleBoardPositions_[0xC], puzzleBoardPositions_[0xD], puzzleBoardPositions_[0xE]),
		// starting from position D
		new PositionJumpPuzzleMove(puzzleBoardPositions_[0xD], puzzleBoardPositions_[0x8], puzzleBoardPositions_[0x4]),
		new PositionJumpPuzzleMove(puzzleBoardPositions_[0xD], puzzleBoardPositions_[0xC], puzzleBoardPositions_[0xB]),
		// starting from position E
		new PositionJumpPuzzleMove(puzzleBoardPositions_[0xE], puzzleBoardPositions_[0x9], puzzleBoardPositions_[0x5]),
		new PositionJumpPuzzleMove(puzzleBoardPositions_[0xE], puzzleBoardPositions_[0xD], puzzleBoardPositions_[0xC])
	};

	int num_moves = sizeof(all_moves) / sizeof(PositionJumpPuzzleMove *);

	for (int index = 0; index < num_moves; ++index)
	{
		allPossibleMoves_.push_back(all_moves[index]);
	}

	ConfigureBoard(occupiedPositions);
}

std::string TrianglePuzzleBoard::ToString()
{
	return "TODO";
}