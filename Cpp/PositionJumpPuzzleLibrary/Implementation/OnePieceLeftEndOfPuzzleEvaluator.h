#ifndef _ONEPEICELEFTENDOFPUZZLEEVALUATOR_H
#define _ONEPEICELEFTENDOFPUZZLEEVALUATOR_H

#include "IEndOfPuzzleEvaluator.h"

class OnePieceLeftEndOfPuzzleEvaluator : public IEndOfPuzzleEvaluator
{
public:
	OnePieceLeftEndOfPuzzleEvaluator() {};
	virtual ~OnePieceLeftEndOfPuzzleEvaluator() {};

	virtual EndOfPuzzlePossibility Evaluate(IPuzzleBoard * puzzleBoard);
	virtual EndOfPuzzlePossibility EvaluateSolution(IPuzzleBoard * puzzleboard);
	
};

#endif