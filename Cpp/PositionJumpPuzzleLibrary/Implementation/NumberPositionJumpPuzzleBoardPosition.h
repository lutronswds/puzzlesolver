#ifndef _NUMBERPOSITIONJUMPPUZZLEBOARDPOSITION_H
#define _NUMBERPOSITIONJUMPPUZZLEBOARDPOSITION_H

#include <sstream>

#include "IPositionJumpPuzzleBoardPosition.h"

class NumberPositionJumpPuzzleBoardPosition : public IPositionJumpPuzzleBoardPosition
{
public:
	NumberPositionJumpPuzzleBoardPosition(int positionNumber) :
		positionNumber_(positionNumber),
		occupied_(false) {}

	virtual ~NumberPositionJumpPuzzleBoardPosition() {};

	virtual bool Occupied() { return occupied_; }
	virtual void SetOccupied(bool occupied) { occupied_ = occupied; }

	// TODO fix this when you know what its supposed to look like
	virtual std::string Description() { return ToString(); }
	virtual std::string ToString()
	{
		std::ostringstream convert;
		convert << positionNumber_;
		return convert.str();
	}

	virtual int PositionNumber() { return positionNumber_; }

private:
	int positionNumber_;
	bool occupied_;
	
};

#endif