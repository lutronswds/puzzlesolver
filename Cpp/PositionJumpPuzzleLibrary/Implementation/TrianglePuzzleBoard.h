#ifndef _TRIANGLE_PUZZLE_BOARD_H
#define _TRIANGLE_PUZZLE_BOARD_H

#include "PositionJumpPuzzleBoardBase.h"

class TrianglePuzzleBoard : public PositionJumpPuzzleBoardBase
{
public:

	enum { NUM_POSITIONS = 15 };

	virtual ~TrianglePuzzleBoard() {};

	virtual int NumberOfPuzzleBoardPositions() { return NUM_POSITIONS; }

	virtual void Initialize(std::list<bool> *occupiedPositions);

	virtual std::string ToString();

	
};

#endif