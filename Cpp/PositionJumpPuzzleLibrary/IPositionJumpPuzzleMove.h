#ifndef _IPOSITIONJUMPPUZZLEMOVE_H
#define _IPOSITIONJUMPPUZZLEMOVE_H

#include "IPuzzleMove.h"
#include "IPositionJumpPuzzleBoardPosition.h"

class IPositionJumpPuzzleMove : public IPuzzleMove
{
public:
	virtual ~IPositionJumpPuzzleMove() {};

	virtual IPositionJumpPuzzleBoardPosition * StartPosition() = 0;
	virtual IPositionJumpPuzzleBoardPosition * MiddlePosition() = 0;
	virtual IPositionJumpPuzzleBoardPosition * EndPosition() = 0;
	virtual bool IsValid() = 0;
};


#endif